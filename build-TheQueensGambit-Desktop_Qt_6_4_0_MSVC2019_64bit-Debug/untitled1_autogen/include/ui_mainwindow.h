/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 6.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QFrame *frame_6;
    QLabel *label;
    QFrame *frame;
    QPushButton *Morpion_button;
    QFrame *frame_2;
    QPushButton *Puissance4_button;
    QFrame *frame_3;
    QPushButton *Othello_button;
    QFrame *frame_4;
    QPushButton *JeuDame_button;
    QFrame *frame_5;
    QPushButton *quitter_button;
    QStatusBar *statusbar;
    QMenuBar *menubar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName("MainWindow");
        MainWindow->resize(225, 300);
        MainWindow->setMinimumSize(QSize(225, 300));
        MainWindow->setMaximumSize(QSize(225, 300));
        MainWindow->setStyleSheet(QString::fromUtf8("background-color:  rgb(24, 49, 73);"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName("centralwidget");
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName("verticalLayout");
        frame_6 = new QFrame(centralwidget);
        frame_6->setObjectName("frame_6");
        frame_6->setMinimumSize(QSize(0, 50));
        frame_6->setFrameShape(QFrame::StyledPanel);
        frame_6->setFrameShadow(QFrame::Raised);
        label = new QLabel(frame_6);
        label->setObjectName("label");
        label->setGeometry(QRect(20, 10, 101, 31));
        label->setStyleSheet(QString::fromUtf8("color : white;\n"
"font : bold 20px;\n"
"text-decoration: underline;"));

        verticalLayout->addWidget(frame_6);

        frame = new QFrame(centralwidget);
        frame->setObjectName("frame");
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        Morpion_button = new QPushButton(frame);
        Morpion_button->setObjectName("Morpion_button");
        Morpion_button->setGeometry(QRect(0, 0, 204, 35));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Morpion_button->sizePolicy().hasHeightForWidth());
        Morpion_button->setSizePolicy(sizePolicy);
        Morpion_button->setMinimumSize(QSize(204, 35));
        Morpion_button->setMaximumSize(QSize(204, 35));
        QFont font;
        font.setBold(false);
        font.setItalic(false);
        Morpion_button->setFont(font);
        Morpion_button->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(0, 0, 0, 128), stop:1 rgba(0, 0, 0, 134));\n"
"    border-style: outset;\n"
"    border-width: 1px;\n"
"    border-radius: 10px;\n"
"    border-color: white;\n"
"    font: 14px;\n"
"	color : white;\n"
"	padding : 3px;\n"
"    min-width: 10em;\n"
"    padding: 6px;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"	color : black;\n"
"	background-color : white;\n"
"}"));

        verticalLayout->addWidget(frame);

        frame_2 = new QFrame(centralwidget);
        frame_2->setObjectName("frame_2");
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        Puissance4_button = new QPushButton(frame_2);
        Puissance4_button->setObjectName("Puissance4_button");
        Puissance4_button->setGeometry(QRect(0, 0, 204, 35));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(Puissance4_button->sizePolicy().hasHeightForWidth());
        Puissance4_button->setSizePolicy(sizePolicy1);
        Puissance4_button->setMinimumSize(QSize(204, 35));
        Puissance4_button->setMaximumSize(QSize(204, 35));
        Puissance4_button->setFont(font);
        Puissance4_button->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(0, 0, 0, 128), stop:1 rgba(0, 0, 0, 134));\n"
"    border-style: outset;\n"
"    border-width: 1px;\n"
"    border-radius: 10px;\n"
"    border-color: white;\n"
"    font: 14px;\n"
"	color : white;\n"
"	padding : 3px;\n"
"    min-width: 10em;\n"
"    padding: 6px;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"	color : black;\n"
"	background-color : white;\n"
"}"));

        verticalLayout->addWidget(frame_2);

        frame_3 = new QFrame(centralwidget);
        frame_3->setObjectName("frame_3");
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        Othello_button = new QPushButton(frame_3);
        Othello_button->setObjectName("Othello_button");
        Othello_button->setGeometry(QRect(0, 0, 204, 35));
        sizePolicy1.setHeightForWidth(Othello_button->sizePolicy().hasHeightForWidth());
        Othello_button->setSizePolicy(sizePolicy1);
        Othello_button->setMinimumSize(QSize(204, 35));
        Othello_button->setMaximumSize(QSize(204, 35));
        Othello_button->setFont(font);
        Othello_button->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(0, 0, 0, 128), stop:1 rgba(0, 0, 0, 134));\n"
"    border-style: outset;\n"
"    border-width: 1px;\n"
"    border-radius: 10px;\n"
"    border-color: white;\n"
"    font: 14px;\n"
"	color : white;\n"
"	padding : 3px;\n"
"    min-width: 10em;\n"
"    padding: 6px;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"	color : black;\n"
"	background-color : white;\n"
"}"));

        verticalLayout->addWidget(frame_3);

        frame_4 = new QFrame(centralwidget);
        frame_4->setObjectName("frame_4");
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Raised);
        JeuDame_button = new QPushButton(frame_4);
        JeuDame_button->setObjectName("JeuDame_button");
        JeuDame_button->setGeometry(QRect(0, 0, 204, 35));
        sizePolicy1.setHeightForWidth(JeuDame_button->sizePolicy().hasHeightForWidth());
        JeuDame_button->setSizePolicy(sizePolicy1);
        JeuDame_button->setMinimumSize(QSize(192, 35));
        JeuDame_button->setMaximumSize(QSize(204, 35));
        JeuDame_button->setFont(font);
        JeuDame_button->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(0, 0, 0, 128), stop:1 rgba(0, 0, 0, 134));\n"
"    border-style: outset;\n"
"    border-width: 1px;\n"
"    border-radius: 10px;\n"
"    border-color: white;\n"
"    font: 14px;\n"
"	color : white;\n"
"    min-width: 10em;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"	color : black;\n"
"	background-color : white;\n"
"}"));

        verticalLayout->addWidget(frame_4);

        frame_5 = new QFrame(centralwidget);
        frame_5->setObjectName("frame_5");
        frame_5->setFrameShape(QFrame::StyledPanel);
        frame_5->setFrameShadow(QFrame::Raised);
        quitter_button = new QPushButton(frame_5);
        quitter_button->setObjectName("quitter_button");
        quitter_button->setGeometry(QRect(0, 0, 100, 35));
        QSizePolicy sizePolicy2(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(quitter_button->sizePolicy().hasHeightForWidth());
        quitter_button->setSizePolicy(sizePolicy2);
        quitter_button->setMinimumSize(QSize(100, 35));
        quitter_button->setMaximumSize(QSize(100, 35));
        quitter_button->setFont(font);
        quitter_button->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(0, 0, 0, 128), stop:1 rgba(0, 0, 0, 134));\n"
"    border-style: outset;\n"
"    border-width: 1px;\n"
"    border-radius: 10px;\n"
"    border-color: white;\n"
"    font: 11px;\n"
"	color : white;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"	color : black;\n"
"	background-color : white;\n"
"}"));

        verticalLayout->addWidget(frame_5);

        MainWindow->setCentralWidget(centralwidget);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName("statusbar");
        MainWindow->setStatusBar(statusbar);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName("menubar");
        menubar->setGeometry(QRect(0, 0, 225, 17));
        MainWindow->setMenuBar(menubar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "QT Games", nullptr));
        Morpion_button->setText(QCoreApplication::translate("MainWindow", "Morpion", nullptr));
        Puissance4_button->setText(QCoreApplication::translate("MainWindow", "Puissance 4", nullptr));
        Othello_button->setText(QCoreApplication::translate("MainWindow", "Othello", nullptr));
        JeuDame_button->setText(QCoreApplication::translate("MainWindow", "Jeu de Dames", nullptr));
        quitter_button->setText(QCoreApplication::translate("MainWindow", "Quitter", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
