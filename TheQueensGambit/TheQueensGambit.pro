#-------------------------------------------------
#
# Project created by QtCreator 2019-04-21T17:21:06
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TheQueensGambit
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    Models/Checkers.cpp \
    Models/Game.cpp \
    Models/Grid.cpp \
    Views/GameView.cpp \
    Views/TicTacToeView.cpp \
    main.cpp \
    Views/mainwindow.cpp \
    Models/Morpion.cpp \
    Models/Othello.cpp \
    Models/Player.cpp \
    Models/Puissance4.cpp \
    Controllers/TerminalDisplay.cpp \
    Controllers/UserInput.cpp \
    Controllers/WinChecker.cpp \
    Controllers/TicTacToeController.cpp

HEADERS += \
    Models/Coordinate.h \
    Models/Checkers.h \
    Models/Direction.h \
    Models/Game.h \
    Views/GameView.h \
    Models/Grid.h \
    IGrid.h \
    IPlayer.h \
    Views/mainwindow.h \
    Models/Morpion.h \
    Models/Othello.h \
    Models/Player.h \
    Models/Point.h \
    Models/Puissance4.h \
    Controllers/TerminalDisplay.h \
    Models/TokenCount.h \
    Controllers/UserInput.h \
    Controllers/WinChecker.h \
    Controllers/TicTacToeController.h \
    Views/TicTacToeView.h


FORMS += \
        mainwindow.ui
