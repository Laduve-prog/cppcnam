#include <vector>
#include "Grid.h"
#include "IPlayer.h"
#include "Coordinate.h"
#ifndef COMPUTERPLAYER_H
#define COMPUTERPLAYER_H
class ComputerPlayer : public IPlayer
{
public:
    //Constructor taking as parameter the identifier of the computer player
    ComputerPlayer(int id) { this->id = id; }

    inline int getId() const { return id; };

    //Method to calculate the move to play for the computer player
    virtual Coordinate playMove(Grid grid) override
    {
        //TODO : Implement the algorithm to calculate the next move to play
        //       for example, using a random strategy or looking for
        //       the best positions to play based on the state of the grid

        //For the example, we return random coordinates
        Coordinate pos;
        do {
            pos.x = rand() % grid.getNbColumns();
            pos.y = rand() % grid.getNbRows();
        } while (grid.checkIfCellIsEmpty(pos) == false);
        return pos;
    }

private:
    int id;
};
#endif //COMPUTERPLAYER_H
