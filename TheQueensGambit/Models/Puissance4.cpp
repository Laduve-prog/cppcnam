#include "Puissance4.h"
#include <iostream>
#include <string>

void Puissance4::playRound(Coordinate coordinates)
{
    int columnNb;
    int tmp = -1;

    //Increment the number of rounds
    round++;

    setCurrentPlayer();


    do {
        std::cout << "It's the player's turn " << currentPlayer.getId() << endl;
        std::cout << "Please enter the column" << endl;
        std::cin >> columnNb;
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        if (cin.fail()) {
            std::cout << "User input is not an integer" << endl;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        else {

            Coordinate c{ 0 , columnNb };
            if (grid.validateCoordinate(c) == true) {
                tmp = grid.checkIfColumIsEmpty(columnNb);
            }
            else {
                std::cout << "The coordinates entered are not valid" << endl;
            }
        }

    } while (tmp == -1);

    //Place the Token in the grid
    Coordinate tokenCoordinate{tmp,columnNb};
    playToken(tokenCoordinate);

    //display grid
    terminalDisplay.displayGrid(grid);

    //Check if the current player has won
    setIsFinished(winChecker.checkWin());

    if (isFinished == true) {
        terminalDisplay.displayWinnerMessage(currentPlayer);
    }

    //Check if there is still empty places in the grid
    if (isFinished == false) {
        setIsFinished(winChecker.checkDraw(round));
    }
}

