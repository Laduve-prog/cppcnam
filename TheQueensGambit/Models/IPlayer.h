#include "Grid.h"
#include "Coordinate.h"
#ifndef IPLAYER_H
#define IPLAYER_H
class IPlayer {
public:
    virtual int getId() const = 0;
    virtual Coordinate useToken(Grid grid) = 0;
    virtual Coordinate checkMove(Grid grid) = 0;
    virtual Coordinate playMove(Grid grid) = 0;
};
#endif //IPLAYER_H