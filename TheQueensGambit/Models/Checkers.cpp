#include "Models/Checkers.h"
#include "Models/Game.h"
#include "Models/Grid.h"
#include "Controllers/WinChecker.h"
#include <vector>
#include <fstream>

void Checkers::playRound(Coordinate coordinates) {
    string resp;
    std::cout << "voulez vous charger la partie ?" << endl;
    std::cin >> resp;
    if (resp == "oui") {
        loadGame();
        terminalDisplay.displayGrid(grid);
    }
    else {
        saveGame();
    }
    round++;
    bool takeToken = false;
    Coordinate startMove;
    Coordinate destMove;
    setCurrentPlayer();
    do {
        do {
            std::string msg1 = "Enter pawn to move";
            terminalDisplay.displayText(msg1);
            startMove = currentPlayer.useToken(grid);
            std::string msg = "Enter destination of the pawn";
            terminalDisplay.displayText(msg);
            destMove = currentPlayer.checkMove(grid);
        } while (grid.validateMove(startMove, currentPlayer.getId(), destMove) == false);
        playToken(startMove, destMove);
        takeToken = grid.takeToken(startMove, destMove);
        terminalDisplay.displayGrid(grid);
    } while (takeToken && possibleTakeNextMove(destMove));

    // Check if the player has won the game
    if (winChecker.endCheckers()) {
        isFinished = true;
        terminalDisplay.displayWinnerMessage(currentPlayer);
    }
}
void Checkers::playToken(Coordinate start, Coordinate destination) {
    vector<vector<Token>>tab = grid.getGrid();
    if (tab[start.x][start.y].isDame()) {
        this->grid.moveToken(start, Token{ currentPlayer.getId(), true }, destination);
    }
    else {
        this->grid.moveToken(start, Token{ currentPlayer.getId(), tokenBecomeDame(destination) }, destination);
    }
}
void Checkers::defaultPlacement() {
    // Place X tokens
    for (int i = 0; i < grid.getNbRows(); i++) {
        for (int j = 0; j < grid.getNbColumns(); j++) {
            if ((i + j) % 2 == 1 && i < 4) {
                Coordinate posToken{ i , j };
                grid.placeToken(posToken, Token{1, false});
            }
        }
    }

    // Place O tokens
    for (int i = 0; i < grid.getNbRows(); i++) {
        for (int j = 0; j < grid.getNbColumns(); j++) {
            if ((i + j) % 2 == 1 && i >= grid.getNbRows() - 4) {
                Coordinate posToken{ i , j };
                grid.placeToken(posToken, Token{ 2, false });
            }
        }
    }

}
bool Checkers::tokenBecomeDame(Coordinate destination) {
    if (currentPlayer.getId() == 1) {
        if (destination.x == 9) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        if (destination.x == 0) {
            return true;
        }
        else {
            return false;
        }
    }
}
void Checkers::possibleMove(Coordinate start) {
    for (int i = 0; i < grid.getNbRows(); i++) {
        for (int j = 0; j < grid.getNbColumns(); j++) {
            if (grid.validateMove(start, currentPlayer.getId(), Coordinate{ i, j }) && 
                (grid.checkIfCellIsEmpty(Coordinate{ i, j }) == true)) {
                possibleMoveList.push_back({ i, j });
            }
        }
    }
}
bool Checkers::possibleTakeNextMove(Coordinate start) {
    for (int i = 0; i < grid.getNbRows(); i++) {
        for (int j = 0; j < grid.getNbColumns(); j++) {
            if (grid.possibleTake(start, currentPlayer.getId(), Coordinate{ i, j }) &&
                (grid.checkIfCellIsEmpty(Coordinate{ i, j }) == true)) {
                return true;
            }
        }
    }
    return false;
}
// Save the current game state to disk
void Checkers::saveGame() {
    // Open a file stream to save the game state
    std::ofstream saveFile("checkers_game_state.bin", ios::binary);
    if (saveFile.is_open()) {
        // Write the current round number to the file
        saveFile.write((char*)&round, sizeof(int));
        // Write the current player id to the file
        int nbround = round;
        saveFile.write((char*)&nbround, sizeof(int));

        // Write the grid state to the file
        vector<vector<Token>> gridState = grid.getGrid();
        for (int i = 0; i < grid.getNbColumns(); i++) {
            for (int j = 0; j < grid.getNbRows(); j++) {
                int playerId = gridState[i][j].getPlayerId();
                bool isDame = gridState[i][j].isDame();
                saveFile.write((char*)&playerId, sizeof(int));
                saveFile.write((char*)&isDame, sizeof(bool));
            }
        }

        // Close the file stream
        saveFile.close();
    }
    else {
        // If the file couldn't be opened, print an error message
        cout << "Error: Could not save game state." << endl;
    }
}

// Load a saved game state from disk
void Checkers::loadGame() {
    // Open a file stream to load the game state
    ifstream loadFile("checkers_game_state.bin", ios::binary);
    if (loadFile.is_open()) {
        // Read the current round number from the file
        loadFile.read((char*)&round, sizeof(int));
        // Read the current player id from the file
        int nbround;
        loadFile.read((char*)&nbround, sizeof(int));
        round = nbround;

        // Read the grid state from the file
        for (int i = 0; i < grid.getNbColumns(); i++) {
            for (int j = 0; j < grid.getNbRows(); j++) {
                int tokenId;
                bool isDame;
                loadFile.read((char*)&tokenId, sizeof(int));
                loadFile.read((char*)&isDame, sizeof(bool));
                Coordinate posToken{ i , j };
                grid.placeToken(posToken, Token{ tokenId, isDame });
            }
        }

        // Close the file stream
        loadFile.close();
    }
    else {
        // If the file couldn't be opened, print an error message
        cout << "Error: Could not load game state." << endl;
    }
}
