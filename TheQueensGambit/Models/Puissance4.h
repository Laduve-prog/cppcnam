#include "Game.h"
#include "Grid.h"

#ifndef PUISSANCE4_H
#define PUISSANCE4_H

class Puissance4 : public Game
{
public:
    Puissance4() :Game(7, 4, 4) {};
    virtual void playRound(Coordinate coordinates) override;
};

#endif //PUISSANCE4_H
