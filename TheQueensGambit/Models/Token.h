#ifndef TOKEN_H
#define TOKEN_H
class Token {
public:
	Token(int playerId, bool dame /*Coordinate coordinate*/) {
		this->dame = dame;
		this->playerId = playerId;
		//this->coordinate = coordinate;
	};
	bool isDame() const { return dame; }
	int getPlayerId() const { return playerId; }
	void setDame() { dame = true; }
	//Coordinate getCoordinate() { return coordinate; }
/*	bool operator== (const Token& other) const {
		return playerId == other.playerId;
	}
	bool operator!= (const Token& other) const {
		return playerId != other.playerId;
	}
	*/
private:
	bool dame;
	int playerId;
	//Coordinate coordinate;
};

#endif // TOKEN_H
