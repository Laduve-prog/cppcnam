#include "Game.h"
#include "Grid.h"
#include "Player.h"
#include <vector>
#include "Controllers/TerminalDisplay.h"
#include "Coordinate.h"
#include <QPushButton>
#include <QObject>
using namespace std;

//Game class constructor
Game::Game(int xGridSize, int yGridSize, int winCondition) : grid(xGridSize, yGridSize), winChecker(winCondition, grid) {
    round = 0;
    players.push_back(Player(1));
    players.push_back(Player(2));
    setCurrentPlayer();
}

//Launch the game
void Game::launch() {

    do {
        currentPlayer.getId();
    } while (isFinished == false);
}

void Game::playToken(Coordinate c) {
    this->grid.placeToken(c, Token{ currentPlayer.getId(), false });
}
//Change current player
void Game::setCurrentPlayer() {

    //manage currentPlayer
    if (round % 2 == 1) {
        currentPlayer = getPlayers()[0];
    }
    else {
        currentPlayer = getPlayers()[1];
    }
}

void Game::cellButtonClicked(){
}



