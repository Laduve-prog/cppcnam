#include <vector>
#include <iostream>
#include "Player.h"
#include "Grid.h"
#include "Controllers/WinChecker.h"
#include "Controllers/UserInput.h"
#include "Controllers/TerminalDisplay.h"

#ifndef GAME_H
#define GAME_H

class Game
{
public:
    Game(int xGridSize, int yGridSize, int winCondition);
    void launch();
    void cellButtonClicked();

protected:
    Grid grid;
    Player currentPlayer;
    bool isFinished;
    int round;
    WinChecker winChecker;
    TerminalDisplay terminalDisplay;
    UserInput userInput;

    virtual void playRound(Coordinate coordinates) = 0;

    std::vector<Player> getPlayers() { return players; };
    const Grid& getGrid() const { return grid; };
    int getNbRounds() const { return round; };

    void setCurrentPlayer();
    void setIsFinished(bool b) { isFinished = b; }
    virtual void playToken(Coordinate c);

private:
    std::vector<Player> players;
};

#endif // GAME_H
