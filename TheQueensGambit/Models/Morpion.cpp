#include "Morpion.h"
#include <iostream>
#include <string>



void Morpion::playRound(Coordinate coordinates) {
    //Increment the number of rounds
    round++;
    setCurrentPlayer();

    //Place the Token in the grid
    playToken(coordinates);


    //Check if the current player has won
    setIsFinished(winChecker.checkWin());

    if (isFinished == true) {
    }

    //Check if there is still empty places in the grid
    if (isFinished == false) {
        setIsFinished(winChecker.checkDraw(round));
    }
}
