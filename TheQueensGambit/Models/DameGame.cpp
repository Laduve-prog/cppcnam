#include "DameGame.h"
#include "Game.h"
#include "Grid.h"
#include "Controllers/WinChecker.h"
#include <vector>

void DameGame::playRound(Coordinate coordinates) {
    round++;
    bool takeToken = false;
    Coordinate startMove;
    Coordinate destMove;
    setCurrentPlayer();
    do {
        do {
            std::string msg1 = "Enter pawn to move";
            terminalDisplay.displayText(msg1);
            startMove = currentPlayer.useToken(grid);
            std::string msg = "Enter destination of the pawn";
            terminalDisplay.displayText(msg);
            destMove = currentPlayer.checkMove(grid);
        } while (grid.validateMove(startMove, currentPlayer.getId(), destMove) == false);
        playToken(startMove, destMove);
        grid.takeToken(startMove, destMove);
        terminalDisplay.displayGrid(grid);
    } while (isFinished == false && possibleTakeNextMove(destMove));

    // Check if the player has won the game
    if (winChecker.endDameGame()) {
        isFinished = true;
        terminalDisplay.displayWinnerMessage(currentPlayer);
    }
}
void DameGame::playToken(Coordinate start, Coordinate destination) {
    vector<vector<Token>>tab = grid.getGrid();
    if (tab[start.x][start.y].isDame()) {
        this->grid.moveToken(start, Token{ currentPlayer.getId(), true }, destination);
    }
    else {
        this->grid.moveToken(start, Token{ currentPlayer.getId(), tokenBecomeDame(destination) }, destination);
    }
}
void DameGame::defaultPlacement() {
    // Place X tokens
    for (int i = 0; i < grid.getNbRows(); i++) {
        for (int j = 0; j < grid.getNbColumns(); j++) {
            if ((i + j) % 2 == 1 && i < 4) {
                Coordinate posToken{ i , j };
                grid.placeToken(posToken, Token{1, false});
            }
        }
    }

    // Place O tokens
    for (int i = 0; i < grid.getNbRows(); i++) {
        for (int j = 0; j < grid.getNbColumns(); j++) {
            if ((i + j) % 2 == 1 && i >= grid.getNbRows() - 4) {
                Coordinate posToken{ i , j };
                grid.placeToken(posToken, Token{ 2, false });
            }
        }
    }

}
bool DameGame::tokenBecomeDame(Coordinate destination) {
    if (currentPlayer.getId() == 1) {
        if (destination.x == 9) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        if (destination.x == 0) {
            return true;
        }
        else {
            return false;
        }
    }
}
void DameGame::possibleMove(Coordinate start) {
    for (int i = 0; i < grid.getNbRows(); i++) {
        for (int j = 0; j < grid.getNbColumns(); j++) {
            if (grid.validateMove(start, currentPlayer.getId(), Coordinate{ i, j }) && 
                (grid.checkIfCellIsEmpty(Coordinate{ i, j }) == true)) {
                possibleMoveList.push_back({ i, j });
            }
        }
    }
}
bool DameGame::possibleTakeNextMove(Coordinate start) {
    for (int i = 0; i < grid.getNbRows(); i++) {
        for (int j = 0; j < grid.getNbColumns(); j++) {
            if (grid.possibleTake(start, currentPlayer.getId(), Coordinate{ i, j }) &&
                (grid.checkIfCellIsEmpty(Coordinate{ i, j }) == true)) {
                return true;
            }
        }
    }
    return false;
}
