#include "Othello.h"
#include "Game.h"
#include "Grid.h"
#include "Controllers/WinChecker.h"
#include <vector>


 // Override the playRound function to implement the Othello game rules
void Othello::playRound(Coordinate coordinates) {

	//On incremente le nombre de round
	round++;
	setCurrentPlayer();

	// Get the player's move
	Coordinate pMove;
	do {
		std::string msg = "Your pawn must be adjacent to an opposing pawn";
		terminalDisplay.displayText(msg);
		pMove = currentPlayer.playMove(grid);
	} while (grid.isValidMove(pMove, currentPlayer.getId()) == false);
	playToken(pMove);

	

	
	//Place the player's piece on the board and flip any captures pieces
	grid.flipTokens(pMove, currentPlayer.getId());

	// Display the game board
	terminalDisplay.displayGrid(grid);

	// Check if the player has won the game
	if (winChecker.checkForWin()) {
		isFinished = true;
		terminalDisplay.displayWinnerMessage(currentPlayer);
	}
}
