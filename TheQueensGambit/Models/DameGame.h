#include "Grid.h"
#include <vector>
#include "Player.h"
#include "Game.h"
#ifndef DAME_GAME_H
#define DAME_GAME_H

class DameGame : public Game {
public:
	DameGame() :Game(10, 10, 5) {
		defaultPlacement();
	};
    virtual void playRound(Coordinate coordinates) override;
    virtual void playToken(Coordinate c, Coordinate destination);
	void defaultPlacement();
	bool tokenBecomeDame(Coordinate destination);
	void possibleMove(Coordinate start);
	bool possibleTakeNextMove(Coordinate start);
private:
	vector<Coordinate> possibleMoveList;
};

#endif // DAME_GAME_H
