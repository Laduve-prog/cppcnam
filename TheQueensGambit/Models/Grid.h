#ifndef GRID_H
#define GRID_H
#include <vector>
#include "Coordinate.h"
#include "TokenCount.h"
#include "Token.h"
using namespace std;

//Grid management class: Allows writing and reading values from the array
class Grid {
public:
    Grid(int xGridSize, int yGridSize);

    inline int getNbRows() const { return nbRows; }
    inline int getNbColumns() const { return nbColumns; }
    const vector<vector<Token>>& getGrid() const { return grid; }

    bool validateMove(Coordinate start, int currentPlayerId, Coordinate destination);

    void moveToken(Coordinate start, Token token, Coordinate destination);
    bool isTokenOwner(Coordinate token, int currentPlayerId) const;
    bool checkIfCellIsEmpty(Coordinate c) const;
    bool validateCoordinate(Coordinate c);
    int checkIfColumIsEmpty(int ColumNb) const;

    //Allows you to place a token within the grid
    void placeToken(Coordinate c, Token token);
    void flipTokensInDirection(Coordinate c, int currentPlayerId, int dx, int dy);
    void flipTokens(Coordinate c, int currentPlayerId);
    bool isValidMove(Coordinate c, int currentPlayerId) const;
    bool willTokenBeTurned(Coordinate c, int currentPlayerId, int dx, int dy) const;
    TokenCount countTokens() const;
    void removeToken(Coordinate token);
    bool takeToken(Coordinate start, Coordinate destination);
    bool possibleTake(Coordinate start, int currentPlayerId, Coordinate destination);
private:
    int nbRows;
    int nbColumns;
    vector<vector<Token>> grid;
};
#endif //Grid_H
