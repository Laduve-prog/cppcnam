#include <iostream>
#include <string>
#include "Models/Direction.h"
#include "Models/TokenCount.h"
#include "WinChecker.h"

//Returns if the game is won
bool WinChecker::checkWin() const {
    for (int i = 0; i < grid.getNbColumns(); i++) {
        for (int j = 0; j < grid.getNbRows(); j++) {
            Coordinate c { i , j };
            if (isLinearMatch(c, directions[TOP_RIGHT].value) || isLinearMatch(c, directions[TOP_LEFT].value) || isLinearMatch(c, directions[LEFT].value) ||
                isLinearMatch(c, directions[BOTTOM_LEFT].value) || isLinearMatch(c, directions[BOTTOM_RIGHT].value) || isLinearMatch(c, directions[RIGHT].value) ||
                isLinearMatch(c, directions[BOTTOM].value) || isLinearMatch(c, directions[TOP].value)) {
                return true;
            }
        }
    }

    return false;
}

//Returns if the game ended in a tie
bool WinChecker::checkDraw(int round) const {
    if (round == maxRound) {
        std::cout << "Match nul";
        return true;
    }
    else {
        return false;
    }
}

//Checks if there is a sequence of symbol ( with n symbol = winCondition) from the coordinates in a given direction
bool WinChecker::isLinearMatch(Coordinate coordinate, Coordinate direction) const {
    grid.getGrid();
    vector<vector<Token>>tab = grid.getGrid();
    //Retrieve the value of the box for the check
    int startValue = tab[coordinate.x][coordinate.y].getPlayerId();

    if (startValue == 0) {
        return false;
    }

    // Loop from 1 because we know that the first value is correct
    for (int i = 1; i < winCondition; i++) {
        // Checks if row and column indices are still within array bounds
        if ((coordinate.x + i * direction.x >= 0 && coordinate.x + i * direction.x < grid.getNbColumns()) && (coordinate.y + i * direction.y >= 0 && coordinate.y + i * direction.y < grid.getNbRows())) {
            //Check if different from the start value
            if (tab[coordinate.x + i * direction.x][coordinate.y + i * direction.y].getPlayerId() != startValue) {
                return false;
            }
        }
        else {
            return false;
        }
    }
    return true;
}


bool WinChecker::checkForWin() {
    // Iterate through the grid to check for any empty cells
    for (int i = 0; i < grid.getNbRows(); i++) {
        for (int j = 0; j < grid.getNbColumns(); j++) {
            Coordinate coordinate{ i,j };
            if (grid.checkIfCellIsEmpty(coordinate) && grid.isValidMove(coordinate, 1) || grid.checkIfCellIsEmpty(coordinate) && grid.isValidMove(coordinate, 2)) {
                // If an empty cell is found, the game is not finished
                return false;
            }
        }
    }
    return true;
}

bool WinChecker::endCheckers() const{
    TokenCount tokenCount = grid.countTokens();
    if (tokenCount.xTokens == 0 || tokenCount.oTokens == 0) {
        return true;
    }
    else {
        return false;
    }
}

int WinChecker::getOthelloWinner() const{
    TokenCount tokenCount = grid.countTokens();
    return compareTokenCount(tokenCount.xTokens, tokenCount.oTokens);
}

int WinChecker::compareTokenCount(int xTokens, int oTokens) const{
    // If one player has more Tokens than the other, return true to indicate a win
    if (xTokens > oTokens) {
        return 1;
    }
    else if (oTokens > xTokens) {
        return 2;
    }
    // If the number of Tokens of each color is equal, the game is a draw
    else {
        return 3;
    }
}
