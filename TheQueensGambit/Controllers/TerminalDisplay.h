#include "Models/Grid.h"
#include <iostream>
#include "Models/Player.h"
#ifndef TERMINALDISPLAY_H
#define TERMINALDISPLAY_H

class TerminalDisplay{

public:
	TerminalDisplay() {};
	//Displays the entire grid
	void displayGrid(const Grid grid) const;
	void displayWinnerMessage(Player currentPlayer) const;
	void showGameSelectionMenu();
	void displayErreurSaisie(string msg);
	void displayText(string msg);
};

#endif //TERMINALDISPLAY_H

