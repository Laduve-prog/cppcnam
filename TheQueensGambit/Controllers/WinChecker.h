#pragma once
#include "Models/Grid.h"
#include "Models/TokenCount.h"

#ifndef WINCHECKER_H
#define WINCHECKER_H

class WinChecker {

public:
	WinChecker(int winCondition, Grid grid) : winCondition(winCondition), grid(grid) { maxRound = grid.getNbColumns() * grid.getNbRows(); }
	bool checkWin() const;
	bool checkDraw(int round) const;
	bool checkForWin();
    int getOthelloWinner() const;
    int countTokens(int tokenType) const;
    int compareTokenCount(int xToken, int oToken) const;
    bool endCheckers() const;

private:
	int winCondition;
	Grid grid;
	int maxRound;

	bool isLinearMatch(Coordinate coordinate, Coordinate direction) const;
};

#endif // WINCHECKER_H
