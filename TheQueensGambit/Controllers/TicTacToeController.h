#ifndef TICTACTOECONTROLLER_H
#define TICTACTOECONTROLLER_H

#include "Views/TicTacToeView.h"
#include "Models/Morpion.h"
#include <QMainWindow>
#include <QObject>

class TicTacToeController : public QObject
{
    Q_OBJECT

    public:
        TicTacToeController(QMainWindow *parent){
            view = new TicTacToeView(parent);
            game = new Morpion();

            for (int i = 0; i < view->cellButtons.size(); ++i) {
                //connect(view->cellButtons[i], &QPushButton::clicked, this, &TicTacToeController::cellButtonClicked);
                QObject::connect(view->cellButtons[i], SIGNAL(clicked()),this,SLOT(cellButtonClicked()));
                //view->cellButtons[i]->dumpObjectInfo();
            }

            view->gridLayout->itemAt(1)->widget()->dumpObjectInfo();
        };

        void show(){
            view->show();
        };

    public slots:
        void cellButtonClicked(){
            qDebug() << "On passe";
            Coordinate coordinates;
            coordinates.x = 0;
            coordinates.y = 0;
            game->playRound(coordinates);
        }

    private:
        TicTacToeView* view;
        Morpion* game;
};

#endif // TICTACTOECONTROLLER_H
