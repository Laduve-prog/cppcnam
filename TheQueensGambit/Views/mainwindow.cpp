#include <iostream>
#include "Controllers/TicTacToeController.h"
#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "Models/DameGame.h"
#include "Models/Puissance4.h"
#include "Models/Morpion.h"
#include "Models/Game.h"
#include <vector>
#include "Models/Player.h"
#include "Models/ComputerPlayer.h"
#include "Controllers/UserInput.h"
#include "Models/Othello.h"

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow)
{

    ui->setupUi(this);

    connect(ui->Morpion_button,&QPushButton::clicked,[this](){
        this->launchGame(1);
    });
    connect(ui->Puissance4_button,&QPushButton::clicked,[this](){
        this->launchGame(2);
    });
    connect(ui->Othello_button,&QPushButton::clicked,[this](){
        this->launchGame(3);
    });
    connect(ui->JeuDame_button,&QPushButton::clicked,[this](){
        this->launchGame(4);
    });
    connect(ui->quitter_button, SIGNAL(clicked()), qApp, SLOT(quit()));

}


void MainWindow::launchGame(int gameIndex){
    std::unique_ptr<TicTacToeController> controller;

    int nbRows;
    int nbCols;

    switch(gameIndex) {
        case (1):
            controller=std::make_unique<TicTacToeController>(this);
            nbRows=3;
            nbCols=3;
            break;
        case (2):
            //game=std::make_unique<Puissance4>();
            //nbRows=4;
            //nbCols=7;
            break;
        case (3):
            //game=std::make_unique<Othello>();
            //nbRows=8;
            //nbCols=8;
            break;
        case (4):
            //game=std::make_unique<DameGame>();
            //nbRows=10;
            //nbCols=10;
            break;
    }
    //showGame(nbRows,nbCols);
    controller->show();
}

MainWindow::~MainWindow()
{
    delete ui;
}
