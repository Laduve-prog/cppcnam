#ifndef TICTACTOEVIEW_H
#define TICTACTOEVIEW_H

#include <QtWidgets>
#include <QGridLayout>
#include <QPushButton>
#include <QButtonGroup>
#include <vector>
#include <QObject>


class TicTacToeView : public QWidget
{
    Q_OBJECT
    public:
        TicTacToeView(QWidget *parent = nullptr);
        QGridLayout *gridLayout;
        std::vector<QPushButton*> cellButtons;
        QButtonGroup *cellButtonGroup;
};

#endif // TICTACTOEVIEW_H
