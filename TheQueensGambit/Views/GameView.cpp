#include "GameView.h"
GameView::GameView(QWidget *parent)
    : QMainWindow(parent)
{
    boardLayout = new QGridLayout;
    setCentralWidget(new QWidget);
    centralWidget()->setLayout(boardLayout);
}

void GameView::createBoard(int rows, int columns)
{
    boardButtons.resize(rows);
    for (int i = 0; i < rows; ++i) {
        boardButtons[i].resize(columns);
        for (int j = 0; j < columns; ++j) {
            boardButtons[i][j] = new QPushButton;
            boardLayout->addWidget(boardButtons[i][j], i, j);
            connect(boardButtons[i][j], &QPushButton::clicked, [this, i, j] { cellButtonClicked(i, j); });
        }
    }
}

