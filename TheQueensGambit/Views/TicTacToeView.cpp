#include "tictactoeview.h"


TicTacToeView::TicTacToeView(QWidget *parent) : QWidget(parent) {
    parent->setMinimumSize(QSize(350, 300));
    this->setMinimumSize(QSize(350, 300));
    gridLayout = new QGridLayout(this);

    //Ajout des boutons pour les cases du jeu
    for (int row = 0; row < 3; row++){
        for (int col = 0; col < 3 ; col++){
            QPushButton *cellButton = new QPushButton;
            cellButton->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
            cellButtons.push_back(cellButton);
            gridLayout->addWidget(cellButton,row,col);
        }
    }

    QPushButton *returnButton = new QPushButton("Retour menu", parent);
            connect(returnButton, &QPushButton::clicked, [parent, this](){
                parent->show();
                this->hide();
     });

    this->setStyleSheet("QWidget { background-color: rgb(24, 49, 73);"
                                                    "color: white;"
                                                    "width: auto;"
                                                    "height: auto;"
                                                 "}"
                                                "QPushButton {"
                                                    "background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(0, 0, 0, 128), stop:1 rgba(0, 0, 0, 134));"
                                                    "border-style: outset;"
                                                    "border-width: 1px;"
                                                    "border-radius: 10px;"
                                                    "border-color: white;"
                                                    "font: 14px;"
                                                    "color : white;"
                                                    "min-width : 100px;"
                                                    "max-width : 100px;"
                                                    "min-height : 30px;"
                                                    "max-height : 3Opx;"
                                                 "}"

                                                 "QPushButton:hover {"
                                                    "color : black;"
                                                    "background-color : white;"
                                                 "}");
                    returnButton->setStyleSheet("QPushButton {background-color : rgb(108,0,0);"
                                                "}"
                                                "QPushButton:hover {background-color : rgb(217,30,24);"
                                                "color : white;}"

                                                );
                    gridLayout->addWidget(returnButton);
                    this->setLayout(gridLayout);
}
