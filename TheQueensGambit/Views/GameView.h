#ifndef GAMEVIEW_H
#define GAMEVIEW_H

#include <QMainWindow>
#include <QGridLayout>
#include <QPushButton>

class GameView : public QMainWindow
{
    Q_OBJECT

public:
    GameView(QWidget *parent = nullptr);

    void createBoard(int rows, int columns);
    virtual void cellButtonClicked(int row, int column) = 0;


private:
    QGridLayout *boardLayout;
    QVector<QVector<QPushButton*>> boardButtons;
};

#endif // GAMEVIEW_H
