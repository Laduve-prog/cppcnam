#include <iostream>

int calculScoreClassique(int score) {
    switch (score) {
    case 0:
        return 0;
    case 1:
        return 15;
    case 2:
        return 30;
    case 3:
        return 40;
    }
}

void determineScore(int nbEchange1, int nbEchange2) {
    int scoreJoueur1;
    int scoreJoueur2;

    //Comptage point avant r�gles sp�ciales
    if (nbEchange1 < 4 && nbEchange2 < 4) {

        //Score joueurs
        scoreJoueur1 = calculScoreClassique(nbEchange1);
        scoreJoueur2 = calculScoreClassique(nbEchange2);

        std::cout << scoreJoueur1 << " - " << scoreJoueur2;
    }
    else {
        if (nbEchange1 - nbEchange2 >= 2) {
            std::cout << "Joueur 1 gagne le jeu";
        }
        else if (nbEchange2 - nbEchange1 >= 2) {
            std::cout << "Joueur 2 gagne le jeu";
        }
        else if (nbEchange1 - nbEchange2 == 1) {
            std::cout << "A - 40";
        }
        else if (nbEchange2 - nbEchange1 == 1) {
            std::cout << "40 - A";
        }
        else {
            std::cout << "40 - 40";
        }
    }
}