#include <iostream>
#include <cstdlib>

void justePrix() {
    int a;
    srand(time(NULL));
    int random = rand() % 1000;
    int i = 0;

    while (true) {
        i++;
        std::cout << "Veuillez saisir un nombre compris entre 0 et 1000 inclus : \n";
        std::cin >> a;

        if (a > random) {
            std::cout << "Trop grand \n";
        }
        else if (a < random) {
            std::cout << "Trop petit \n";
        }
        else {
            std::cout << "Bien joue , vous avez gagne en : " << i << " essais \n";
            return;
        }
    }
}

//En gros une dichotomie
void justePrixIa() {
    bool trouve = false;
    srand(time(NULL));
    int random = rand() % 1000;

    int min = 0;
    int max = 1000;
    int current;
    int i = 0;

    do {
        //Compteur essai
        i++;

        //On prend la valeur au milieu de l'intervalle
        current = (min + max) / 2;
        std::cout << "Guess de l'ordinateur : " << current << "\n";

        if (current == random) {
            trouve = true;
            std::cout << "Bien joue , vous avez gagne en : " << i << " essais \n";
        }
        else if (current < random) {
            min = current + 1;
            std::cout << "Trop petit \n";
        }
        else {
            max = current - 1;
            std::cout << "Trop grand \n";
        }
    } while (trouve == false);
}