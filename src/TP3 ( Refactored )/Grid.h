#ifndef GRID_H
#define GRID_H
#include <vector>
using namespace std;

//Classe de gestion de la grille : Permet l'�criture et la lecture des valeurs du tableau
class Grid {
public:
    Grid() {};
    Grid(int xGridSize, int yGridSize);

    inline int getNbRows() const { return nbRows; }
    inline int getNbColumns() const { return nbColumns; }
    const vector<vector<int>>& getGrid() const { return grid; }

    bool checkIfCellIsEmpty(int xCoordinate, int yCoordinate) const;
    bool validateCoordinate(int x, int y);
    int checkIfColumIsEmpty(int ColumNb) const;

    //Permet de placer un disque au sein de la grille
    void placeDisc(int x, int y, int id);

private:
    int nbRows;
    int nbColumns;
    vector<vector<int>> grid;
};
#endif //Grid_H