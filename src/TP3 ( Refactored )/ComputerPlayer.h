#include <vector>
#include "Grid.h"
#include "IPlayer.h"
#include "Point.h"
#ifndef COMPUTERPLAYER_H
#define COMPUTERPLAYER_H
class ComputerPlayer : IPlayer
{
public:
    //Constructeur prenant en param�tre l'identifiant du joueur ordinateur
    ComputerPlayer(int id) {}

    //M�thode pour calculer le coup � jouer pour le joueur ordinateur
    virtual Point playMove(Grid grid) override
    {
        //TODO : Impl�menter l'algorithme pour calculer le prochain coup � jouer
        //par exemple, en utilisant une strat�gie al�atoire ou en recherchant
        //les meilleures positions pour jouer en fonction de l'�tat de la grille

        //Pour l'exemple, on retourne des coordonn�es al�atoires
        Point pos;
        do {
            pos.x = rand() % grid.getNbColumns();
            pos.y = rand() % grid.getNbRows();
        } while (grid.checkIfCellIsEmpty(pos.x, pos.y) == false);
        return pos;
    }
};
#endif //COMPUTERPLAYER_H
