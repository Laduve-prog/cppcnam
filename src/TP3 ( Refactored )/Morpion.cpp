#include "Morpion.h"
#include <iostream>
#include <string>



void Morpion::playRound() {
    //On incremente le nombre de round
    round++;
    setCurrentPlayer();

    //ask the player x and y coordinates where to play the disc
    Point coordinates = currentPlayer.playMove(grid);


    /*bool coordinatesAreValid = false;
    int xCoordinate;
    int yCoordinate;*/


   /* do {
        bool tmp = false;
        std::cout << "C'est au tour du joueur  " << currentPlayer.getId() << endl;
        std::cout << "Veuillez saisir les coordonnees de la case ( format : x y )" << endl;
        std::cin >> xCoordinate >> yCoordinate;

        if (grid.validateCoordinate(xCoordinate, yCoordinate)) {
            if (grid.checkIfCellIsEmpty(xCoordinate, yCoordinate) == true) {
                coordinatesAreValid = true;
            }
            else {
                std::cout << "La case est deja pleine\n";
            }
        }
        else {
            std::cout << "Les coordonnees saisies ne sont pas valides\n";
        }

    } while (coordinatesAreValid == false);*/

    //On place le disc dans la grille
    //playDisc(xCoordinate, yCoordinate);

    //On place le disc dans la grille
    playDisc(coordinates.x, coordinates.y);

    //display grid
    displayGrid();

    //On v�rifie si le joueur courant a gagn�
    //setIsFinished(winChecker.checkWin(xCoordinate, yCoordinate));

    //On v�rifie si le joueur courant a gagn�
    setIsFinished(winChecker.checkWin(coordinates.x, coordinates.y));

    if (isFinished == true) {
        std::cout << "\n ### Felicitation Joueur " << currentPlayer.getId() << " ###\n\n\n";
    }

    //Check if there is still empty places in the grid
    if (isFinished == false) {
        setIsFinished(winChecker.checkDraw(round));
    }
}