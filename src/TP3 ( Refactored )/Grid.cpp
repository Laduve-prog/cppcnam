#include "Grid.h"
#include <iostream>

Grid::Grid(int xGridSize, int yGridSize) {
    nbRows = xGridSize;
    nbColumns = yGridSize;

    std::vector<std::vector<int>> rowsVector;

    for (int row = 0; row < yGridSize; row++)
    {
        std::vector<int> colsVector;
        for (int col = 0; col < xGridSize; col++)
        {
            colsVector.push_back(0);
        }
        rowsVector.push_back(colsVector);
    }

    this->grid = rowsVector;
}

bool Grid::checkIfCellIsEmpty(int xCoordinate, int yCoordinate) const {
    if (this->grid[xCoordinate][yCoordinate] == 0) {
        return true;
    }
    else {
        return false;
    }
}

void Grid::placeDisc(int x, int y, int id)
{
    grid[x][y] = id;
}

bool Grid::validateCoordinate(int x, int y)
{
    if (x < this->getNbColumns() && y < this->getNbRows()) {
        return true;
    }
    return false;
}

int Grid::checkIfColumIsEmpty(int ColumNb) const
{
    int tmp = -1;
    int i = 0;

    while (i < nbColumns && this->checkIfCellIsEmpty(i, ColumNb) == true) {
        tmp = i;
        i++;
    }

    return tmp;
}
