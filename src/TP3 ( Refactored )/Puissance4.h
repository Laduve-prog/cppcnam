#include "Game.h"
#include "Grid.h"

#ifndef PUISSANCE4_H
#define PUISSANCE4_H

class Puissance4 : public Game
{
public:
    Puissance4(std::vector<Player> players) :Game(7, 4, 4, players) {};

    virtual void playRound() override;

private:
    const int xGridSize = 7;
    const int yGridSize = 4;
    const int winCondition = 4;

};

#endif //PUISSANCE4_H