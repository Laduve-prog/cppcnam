#include <vector>
#include <iostream>
#include "Player.h"
#include "Grid.h"
#include "WinChecker.h"

#ifndef GAME_H
#define GAME_H

class Game
{
public:
    Game(int xGridSize, int yGridSize, int winCondition, std::vector<Player> players);
    void launch();

protected:
    Grid grid;
    Player currentPlayer;
    bool isFinished;
    int round;
    WinChecker winChecker;

    virtual void playRound() = 0;

    const std::vector<Player>& getPlayers() const { return players; };
    const Grid& getGrid() const { return grid; };
    int getNbRounds() const { return round; };

    void setCurrentPlayer();
    void setIsFinished(bool b) { this->isFinished = b; }
    void playDisc(int xCoordinate, int yCoordinate);

    //Affiche l'intégralité de la grille
    void displayGrid() const;

private:
    std::vector<Player> players;
};

#endif // GAME_H