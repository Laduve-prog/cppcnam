#include "Puissance4.h"
#include <iostream>
#include <string>

void Puissance4::playRound()
{
    int columNb;
    int tmp = -1;

    //On incremente le nombre de round
    round++;

    setCurrentPlayer();


    do {
        std::cout << "C'est au tour du joueur  " << currentPlayer.getId() << endl;
        std::cout << "Veuillez saisir la colonne" << endl;
        std::cin >> columNb;

        if (grid.validateCoordinate(0, columNb) == true) {
            tmp = grid.checkIfColumIsEmpty(columNb);
        }
        else {
            std::cout << "Les coordonnees saisies ne sont pas valides\n";
        }

    } while (tmp == -1);

    //On place le disc dans la grille
    playDisc(tmp, columNb);

    //display grid
    displayGrid();

    //On v�rifie si le joueur courant a gagn�
    setIsFinished(winChecker.checkWin(tmp, columNb));

    //Check if there is still empty places in the grid
    if (isFinished == false) {
        setIsFinished(winChecker.checkDraw(round));
    }
}

