#include "WinChecker.h"
#include <iostream>
#include <string>

//Renvoie si la partie est gagn�
bool WinChecker::checkWin(int x, int y) const {
    for (int i = 0; i < grid.getNbColumns(); i++) {
        for (int j = 0; j < grid.getNbRows(); j++) {
            if (isLinearMatch(i, j, 1, 0) || isLinearMatch(i, j, 0, 1) || isLinearMatch(i, j, 1, 1) || isLinearMatch(i, j, -1, -1) || isLinearMatch(i, j, -1, 0) || isLinearMatch(i, j, 0, -1) || isLinearMatch(i, j, 1, -1) || isLinearMatch(i, j, -1, 1)) {
                return true;
            }
        }
    }

    return false;
}

//Renvoie si la partie c'est fini sur une �galit�
bool WinChecker::checkDraw(int round) const {
    if (round == maxRound) {
        std::cout << "Match nul";
        return true;
    }
    else {
        return false;
    }
}

//V�rifie si il existe une suite de symbole ( avec n symbole = winCondition) � partir des coordonn�es dans une direction donn�e
bool WinChecker::isLinearMatch(int xCoordinate, int yCoordinate, int stepx, int stepy) const {
    vector<vector<int>>tab = grid.getGrid();
    //On recupere la valeur de la case pour la verification
    int startValue = tab[xCoordinate][yCoordinate];

    if (startValue == 0) {
        return false;
    }

    //On boucle a partir de 1 car on sait que la premiere valeur est correcte
    for (int i = 1; i < winCondition; i++) {
        // Verifie si les indices de ligne et de colonne sont toujours dans les limites du tableau.
        if ((xCoordinate + i * stepx >= 0 && xCoordinate + i * stepx < grid.getNbColumns()) && (yCoordinate + i * stepy >= 0 && yCoordinate + i * stepy < grid.getNbRows())) {
            //On v�rifie si different de la start value
            if (tab[xCoordinate + i * stepx][yCoordinate + i * stepy] != startValue) {
                return false;
            }
        }
        else {
            return false;
        }
    }
    return true;
}