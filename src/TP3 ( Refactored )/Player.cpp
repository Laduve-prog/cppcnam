#include <string>
#include <iostream>
#include "Player.h"
#include "Point.h"
#include "Grid.h"
#include "Game.h"
using namespace std;

Player::Player(int id) {
    this->id = id;
}

Point Player::playMove(Grid grid)
{
    bool coordinatesAreValid = false;
    Point coordinates;
    do {
        std::cout << "C'est au tour du joueur  " << id << endl;
        std::cout << "Veuillez saisir les coordonnees de la case ( format : x y )" << endl;
        std::cin >> coordinates.x >> coordinates.y;

        if (grid.validateCoordinate(coordinates.x, coordinates.y)) {
            if (grid.checkIfCellIsEmpty(coordinates.x, coordinates.y) == true) {
                coordinatesAreValid = true;
            }
            else {
                std::cout << "La case est deja pleine\n";
            }
        }
        else {
            std::cout << "Les coordonnees saisies ne sont pas valides\n";
        }

    } while (coordinatesAreValid == false);
    return coordinates;
}