#include "Game.h"
#include "Grid.h"
#include "Player.h"
#include <vector>
using namespace std;

//Constructeur de la classe Jeu
Game::Game(int xGridSize, int yGridSize, int winCondition, std::vector<Player> players) : players(players), grid(xGridSize, yGridSize), winChecker(winCondition, grid) {
    round = 0;
}

//Lance le jeu
void Game::launch() {
    //display grid
    displayGrid();

    do {
        currentPlayer.getId();
        playRound();
    } while (isFinished == false);
}

void Game::playDisc(int xCoordinate, int yCoordinate) {
    this->grid.placeDisc(xCoordinate, yCoordinate, currentPlayer.getId());
}

//Change le joueur courant
void Game::setCurrentPlayer() {
    //manage currentPlayer
    if (round % 2 == 1) {
        currentPlayer = getPlayers()[0];
    }
    else {
        currentPlayer = getPlayers()[1];
    }
}

//G�re l'affichage de la grille
void Game::displayGrid() const
{
    std::cout << "\n\n";
    std::cout << "### Affichage de la grille ###\n\n";

    for (int i = 0; i < grid.getNbColumns(); i++)
    {
        for (int j = 0; j < grid.getNbRows(); j++)
        {
            if (this->grid.getGrid()[i][j] == 0)
            {
                std::cout << " ";
            }
            else
            {
                if (this->grid.getGrid()[i][j] == 1)
                    std::cout << "X";
                else
                    std::cout << "O";
            }

            if (j == grid.getNbRows() - 1)
            {
                std::cout << std::endl;
            }
            else
            {
                std::cout << " | ";
            }
        }
    }

    std::cout << "\n";
}

