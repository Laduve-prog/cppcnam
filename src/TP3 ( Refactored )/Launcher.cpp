
#include "Puissance4.h"
#include "Morpion.h"
#include <iostream>
#include "Game.h"
#include <vector>
#include "Player.h"

int gameChoice() {

    int x;
    std::cout << "######## Selection du mode de jeu ########\n\n\n";
    std::cout << "Veuillez entrez la valeur qui corresponds au jeu\n";
    std::cout << "Choix du jeu : \n 1: Morpion\n 2: Puissance 4\n Autres: Quittez\n";
    std::cin >> x;

    return x;
}

int main() {
    while (true) {

        int gameIndex = gameChoice();
        Game* game;

        std::vector<Player> players{};
        Player player1(1);
        Player player2(2);
        players.push_back(player1);
        players.push_back(player2);

        if (gameIndex == 1) {
            game = new Morpion(players);
        }
        else if (gameIndex == 2) {
            game = new Puissance4(players);
        }
        else {
            return 0;
        }

        game->launch();
    }
}