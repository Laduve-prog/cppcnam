# CppCnam

Groupe : 337
Membres : GERBERON Alexandre , DUVERNAY Benoît

### Choix du projet

Nous avons choisi de partir de ce projet car malgré le fait qu'il soit pas complet car :

- ce projet semblait mieux respecter les principes de la POO
- Il est plus facilement évolutif et maintenable
- La tâche de refactorisation sera moins lourde en partant de cette base

NB: La refactorisation sera accompagné d'une étape de vérification de la const correctness ainsi que la correction de certains smells.

## S : Single Responsability Principle

Une classe ne doit posséder qu’une et une seule responsabilité.

### Avant Refactorisation:

- La classe Grid gère son management et les opérations sur cette dernière mais également son affichage.

- La classe Game gère la gestion du jeu , mais aussi la vérification des conditions de victoires et des joueurs car cette dernière est responsable du management du jeu et des ses joueurs.

### Après Refactorisation:

- Création d'une classe WinChecker qui va gérer la vérifications des conditions de victoire à la place de game. Instanciation d'un WinChecker dans la classe Game.

- La méthode displayGrid() transféré à la classe Game

## O : Open Close Principle

Une classe, une méthode, un module doit pouvoir être étendu, supporter différentes implémentations (Open for extension) sans pour cela devoir être modifié (closed for modification).

## L : Liskov Substitution Principle

Un utilisateur de la classe de base doit pouvoir continuer de fonctionner correctement si une classe dérivée de la classe principale lui est fournie à la place.

## I : Interface Segregation Principle

Utiliser les interfaces pour définir des contrats, des ensembles de fonctionnalités répondant à un besoin fonctionnel, plutôt que de se contenter d’apporter de l’abstraction à nos classes.

Ajout d'interface : IPlayer et IGrid

## D : Dependency Inversion Principle

les modules de haut niveau ne doivent pas dépendre de modules de plus bas niveau.

Essai d'implémentation d'une interface IPlayer et IGrid pour que la game classe dépende d'abstractions et non plus d'implémentation concrète des class Player et Grid -> objectif inversion de dépendance
