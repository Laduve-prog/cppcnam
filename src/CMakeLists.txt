cmake_minimum_required(VERSION 3.20)

project(cppcnam)

add_executable(tp1-ex1
    TP1/Partie1/main.cpp
    TP1/Partie1/functions.cpp
)

add_executable(tp1-ex2
    TP1/Partie2/main.cpp
    TP1/Partie2/functions.cpp
)

add_executable(tp1-ex3
    TP1/Partie3_1/main.cpp
    TP1/Partie3_1/functions.cpp
)

add_executable(tp1-ex3_2
    TP1/Partie3_2/main.cpp
    TP1/Partie3_2/functions.cpp
)

add_executable(tp2
    TP2/Rectangle.cpp
    TP2/Cercle.cpp
    TP2/Triangle.cpp
    TP2/main.cpp
)

add_executable(tp3
    TP3/Puissance4.cpp
    TP3/Game.cpp
    TP3/Grid.cpp
    TP3/Player.cpp
    TP3/main.cpp
)