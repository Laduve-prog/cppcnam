#include "Grid.h"
#include <iostream>

Grid::Grid(int xGridSize ,int yGridSize){
    nbRows = xGridSize;
    nbColumns = yGridSize;

    std::vector<std::vector<int>> rowsVector;

    for (int row = 0; row < yGridSize; row++)
    {
        std::vector<int> colsVector;
        for (int col = 0; col < xGridSize; col++)
        {
            colsVector.push_back(0);
        }
        rowsVector.push_back(colsVector);
    }

    this->grid = rowsVector;
}

bool Grid::checkIfCellIsEmpty(int xCoordinate , int yCoordinate , int id){
    if (this->grid[xCoordinate][yCoordinate] == 0){
        this->grid[xCoordinate][yCoordinate] = id;
        return true;
    }
    else{
        return false;
    }
}

void Grid::displayGrid() const
{
    
	for (int i=0;i < nbRows;i++)
	{
		for (int j=0;j<nbColumns;j++)
		{
            if (this->grid[i][j] == 0)
            {
                std::cout << " ";
            }
            else
            {
                if (this->grid[i][j] == 1)
                    std::cout << "X";
                else
                    std::cout << "O";
            }

            if (j == nbColumns - 1)
            {
                std::cout << std::endl;
            }
            else
            {
                std::cout << " | ";
            }
		}
	}
}