#include <string>
#include <iostream>
#ifndef PLAYER_H
#define PLAYER_H
class Player{
    public :
        Player(){};
        Player(int id);
        inline int getId(){return id;};
    private :
        int id;
};
#endif //PLAYER_H