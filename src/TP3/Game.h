#include <vector>
#include <iostream>
#include "Player.h"
#include "Grid.h"

#ifndef GAME_H
#define GAME_H

class Game
{
public:
/**
 * @brief Construct a new Game object
 * 
 * @param xGridSize
 * @param yGridSize 
 * @param winCondition 
 * @param players 
 */
    Game(int xGridSize , int yGridSize , int winCondition, std::vector<Player> players);

    std::vector<Player> getPlayers() { return players; };
    Player getCurrentPlayer() { return currentPlayer; };
    Grid getGrid() { return grid; };
    int getNbRounds();

    void setCurrentPlayer(Player currentPlayer) { this->currentPlayer = currentPlayer;};
    void playRound();
    bool checkNbRound();

    void setIsFinished(bool b) { this->isFinished = b; }
    
    /**
     * @brief Check if the player won the game
     * 
     * @param xCoordinate 
     * @param yCoordinate 
     * @return true 
     * @return false 
     */
    bool checkWin(int xCoordinate,int yCoordinate);

    /**
     * @brief ask the plaer to play a disc
     * 
     */
    bool playDisc();

    void launch();

private:
    Grid grid;
    std::vector<Player> players;
    int winCondition = 3;
    Player currentPlayer;
    int maxRound;
    int round;
    bool isFinished;

    /**
     * @brief Method to check if n symbols are aligned in a row
     * 
     * @param xCoordinate 
     * @param yCoordinate 
     * @param stepx 
     * @param stepy 
     * @return true 
     * @return false 
     */
    bool isLinearMatch(int xCoordinate , int yCoordinate , int stepx , int stepy);
};

#endif // GAME_H
