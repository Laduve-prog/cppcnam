#ifndef GRID_H
#define GRID_H
#include <vector>
using namespace std;

class Grid{
    public:
        Grid(){};
        Grid(int xGridSize , int yGridSize);

        int getNbRows() { return nbRows; }
        int getNbColumns() { return nbColumns; }
        vector<vector<int>> getGrid(){return grid;}

        bool checkIfCellIsEmpty(int xCoordinate, int yCoordinate , int id);
        void displayGrid() const;
    private:
        int nbRows;
        int nbColumns;
        vector<vector<int>> grid;
};
#endif //Grid_H