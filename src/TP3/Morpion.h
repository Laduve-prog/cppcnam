
#include "Grid.h"
#include <vector>
#include "Player.h"
#include "Game.h"

#ifndef MORPION_H
#define MORPION_H

class Morpion : public Game
{
    public:
        Morpion(std::vector<Player> players) :Game(3,3,3,players){};
    private:
        const int xGridSize = 3;
        const int yGridSize = 3;
        const int winCondition = 3;
};

#endif //MORPION_H