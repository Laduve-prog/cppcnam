#include "Game.h"
#include "Grid.h"
#include "Player.h"
#include <vector>
using namespace std;

//Game constructor
Game::Game(int xGridSize , int yGridSize , int winCondition , std::vector<Player> players) : players(players) , grid(xGridSize, yGridSize){
    maxRound = xGridSize * yGridSize;
    round = 0;
}

void Game::launch(){
    do{
        currentPlayer.getId();
        playRound();
    } while (isFinished == false);
}

bool Game::checkWin(int x, int y){
    return (isLinearMatch(x,y,1,0) || isLinearMatch(x,y,0,1) || isLinearMatch(x,y,1,1) || isLinearMatch(x,y,1,-1));
}

bool Game::isLinearMatch(int xCoordinate , int yCoordinate , int stepx , int stepy){
    vector<vector<int>>tab = grid.getGrid();

    //On r�cup�re la valeur de la case pour la verification
    int startValue = tab[xCoordinate][yCoordinate];

    //On boucle a partir de 1 car on sait que la premi�re valeur est correcte
    for(int i = 1 ; i < winCondition ; i++){
        if ((xCoordinate + i < grid.getNbColumns() && xCoordinate >= 0) && (yCoordinate + i < grid.getNbRows() && (yCoordinate + i * stepy >= 0))) {
            if (tab[xCoordinate + i * stepx][yCoordinate + i * stepy] != startValue) {
                return false;
            }
        }
        else {
            return false;
        }
    }

    return true;
}

void Game::playRound(){
    round++;
    //display grid
    grid.displayGrid();
    //ask the player x and y coordinates where to play the disc
    setIsFinished(playDisc());
    //Check if there is still empty places in the grid
    if (isFinished == false) {
        setIsFinished(checkNbRound());
    }
}

bool Game::playDisc(){ 

    int xCoordinate;
    int yCoordinate;

    //manage currentPlayer
    if (round % 2 == 1) {
        currentPlayer = getPlayers()[0];
    }
    else {
        currentPlayer = getPlayers()[1];
    }


    do{
        std::cout << "C'est au tour du joueur  " << currentPlayer.getId() << endl;
        std::cout << "Veuillez saisir la position du jeton sur l'axe x" << endl;
        std::cin >> xCoordinate;
        std::cout << "Veuillez saisir la position du jeton sur l'axe y" << endl;
        std::cin >> yCoordinate;
    }
    while(this->grid.checkIfCellIsEmpty(xCoordinate,yCoordinate,currentPlayer.getId()) == false);
    
    //this->grid.getGrid()[xCoordinate][yCoordinate] = currentPlayer.getId();

    //Check if the player has won the round
    return checkWin(xCoordinate,yCoordinate);
}

//Return true if the nb round is equals maxRound
bool Game::checkNbRound(){ 
    if(round == maxRound){
        std::cout << "Match nul";
        return true;
    }
    else{
        return false;
    }
}
