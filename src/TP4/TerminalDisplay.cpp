#include "TerminalDisplay.h"
#include "Grid.h"
#include "Player.h"
#include <iostream>

//Manages the grid display
void TerminalDisplay::displayGrid(const Grid grid) const
{
    std::cout << "\n\n";
    std::cout << "### Grid Display ###\n\n";

    //Display nb
    std::cout << "  ";
    for (int i = 0; i < grid.getNbRows(); i++) {
        std::cout << i << "   ";
    }
    std::cout << std::endl;

    for (int i = 0; i < grid.getNbColumns(); i++)
    {
        //Display nb
        std::cout << i << " ";

        for (int j = 0; j < grid.getNbRows(); j++)
        {
            if (grid.getGrid()[i][j] == 0)
            {
                std::cout << " ";
            }
            else
            {
                if (grid.getGrid()[i][j] == 1)
                    std::cout << "X";
                else
                    std::cout << "O";
            }

            if (j == grid.getNbRows() - 1)
            {
                std::cout << std::endl;
            }
            else
            {
                std::cout << " | ";
            }
        }
    }

    std::cout << "\n";
}

void TerminalDisplay::displayWinnerMessage(Player currentPlayer) const {

    std::cout << "\n ### Congratulations player " << currentPlayer.getId() << " ###\n\n\n";

}

void TerminalDisplay::showGameSelectionMenu() {
    std::cout << "######## Game mode selection ########\n\n\n";
    std::cout << "Please enter the value that corresponds to the game\n";
    std::cout << "Choice of game: \n 1: Tic Tac Toe\n 2: Power 4\n 3: Othello\n Others: Exit\n";
}

void TerminalDisplay::displayErreurSaisie(string msg) {
    std::cout << "Input Error : " << msg << endl;
}

void TerminalDisplay::displayText(string msg) {
    std::cout << msg << endl;
}
