#pragma once
#include "Grid.h"

#ifndef WINCHECKER_H
#define WINCHECKER_H

class WinChecker {

public:
	WinChecker(int winCondition, const Grid& grid) : winCondition(winCondition), grid(grid) { maxRound = grid.getNbColumns() * grid.getNbRows(); }
	bool checkWin(int x, int y) const;
	bool checkDraw(int round) const;
	bool checkForWin();
	int getOthelloWinner();

private:
	int winCondition;
	const Grid& grid;
	int maxRound;

	bool isLinearMatch(int xCoordinate, int yCoordinate, int stepx, int stepy) const;
};

#endif #WINCHECKER_H