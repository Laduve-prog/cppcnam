#include "Grid.h"
#include "Point.h"
#ifndef IPLAYER_H
#define IPLAYER_H
class IPlayer {
public:
    virtual int getId() const = 0;
    virtual Point playMove(Grid grid) = 0;
};
#endif //IPLAYER_H