
#include "Puissance4.h"
#include "Morpion.h"
#include <iostream>
#include "Game.h"
#include <vector>
#include "Player.h"
#include "ComputerPlayer.h"
#include "UserInput.h"
#include "Othello.h"

int main() {
    while (true) {

        TerminalDisplay terminalDisplay;
        UserInput userInput;

        terminalDisplay.showGameSelectionMenu();
        int gameIndex = userInput.getIntValue();

        std::unique_ptr<Game> game;
        std::vector<Player> players{};
        players.push_back(Player(1));
        players.push_back(Player(2));

        if (gameIndex == 1) {
            game = std::make_unique<Morpion>(players);
        }
        else if (gameIndex == 2) {
            game = std::make_unique<Puissance4>(players);
        }
        else if (gameIndex == 3) {
            game = std::make_unique<Othello>(players);
        }
        else {
            return 0;
        }

        game->launch();
    }
}