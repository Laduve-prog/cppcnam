#include "Puissance4.h"
#include <iostream>
#include <string>

void Puissance4::playRound()
{
    int columNb;
    int tmp = -1;

    //Increment the number of rounds
    round++;

    setCurrentPlayer();


    do {
        std::cout << "It's the player's turn " << currentPlayer.getId() << endl;
        std::cout << "Please enter the column" << endl;
        std::cin >> columNb;
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        if (cin.fail()) {
            std::cout << "User input is not an integer\n";
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        else {


            if (grid.validateCoordinate(0, columNb) == true) {
                tmp = grid.checkIfColumIsEmpty(columNb);
            }
            else {
                std::cout << "The coordinates entered are not valid\n";
            }
        }

    } while (tmp == -1);

    //Place the disc in the grid
    playDisc(tmp, columNb);

    //display grid
    terminalDisplay.displayGrid(grid);

    //Check if the current player has won
    setIsFinished(winChecker.checkWin(tmp, columNb));

    if (isFinished == true) {
        terminalDisplay.displayWinnerMessage(currentPlayer);
    }

    //Check if there is still empty places in the grid
    if (isFinished == false) {
        setIsFinished(winChecker.checkDraw(round));
    }
}

