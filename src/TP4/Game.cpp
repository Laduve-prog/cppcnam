#include "Game.h"
#include "Grid.h"
#include "Player.h"
#include <vector>
#include "TerminalDisplay.h"
using namespace std;

//Game class constructor
Game::Game(int xGridSize, int yGridSize, int winCondition, std::vector<Player> players) : players(players), grid(xGridSize, yGridSize), winChecker(winCondition, grid) {
    terminalDisplay = TerminalDisplay();
    round = 0;
    setCurrentPlayer();
}

//Launch the game
void Game::launch() {
    //display grid
    terminalDisplay.displayGrid(grid);

    do {
        currentPlayer.getId();
        playRound();
    } while (isFinished == false);
}

void Game::playDisc(int xCoordinate, int yCoordinate) {
    this->grid.placeDisc(xCoordinate, yCoordinate, currentPlayer.getId());
}

//Change current player
void Game::setCurrentPlayer() {

    //manage currentPlayer
    if (round % 2 == 1) {
        currentPlayer = getPlayers()[0];
    }
    else {
        currentPlayer = getPlayers()[1];
    }
}



