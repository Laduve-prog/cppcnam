#ifndef GRID_H
#define GRID_H
#include <vector>
#include "Point.h"
using namespace std;

//Grid management class: Allows writing and reading values from the array
class Grid {
public:
    Grid() {};
    Grid(int xGridSize, int yGridSize);

    inline int getNbRows() const { return nbRows; }
    inline int getNbColumns() const { return nbColumns; }
    const vector<vector<int>>& getGrid() const { return grid; }

    bool checkIfCellIsEmpty(int xCoordinate, int yCoordinate) const;
    bool validateCoordinate(int x, int y);
    int checkIfColumIsEmpty(int ColumNb) const;

    //Allows you to place a disc within the grid
    void placeDisc(int x, int y, int id);
    void flipDiscsInDirection(Point p, int currentPlayerId, int dx, int dy);
    void flipDiscs(Point p, int currentPlayerId);
    bool isValidMove(Point p, int currentPlayerId) const;
    bool willTokenBeTurned(Point p, int currentPlayerId, int dx, int dy) const;
private:
    int nbRows;
    int nbColumns;
    vector<vector<int>> grid;
};
#endif //Grid_H