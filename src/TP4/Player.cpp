#include <string>
#include <iostream>
#include "Player.h"
#include "Point.h"
#include "Grid.h"
#include "Game.h"
#include "sstream"
using namespace std;

Player::Player(int id) {
    this->id = id;
}

Point Player::playMove(Grid grid)
{
    bool coordinatesAreValid = false;
    Point coordinates;
    do {
        std::cout << "It's the player's turn " << id << endl;
        std::cout << "Please enter the coordinates( format: x y )" << endl;
        std::cin >> coordinates.x >> coordinates.y;
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        if (cin.fail()) {
            std::cout << "User input is not an integer\n";
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        else {
            if (grid.validateCoordinate(coordinates.x, coordinates.y)) {
                if (grid.checkIfCellIsEmpty(coordinates.x, coordinates.y) == true) {
                    coordinatesAreValid = true;
                }
                else {
                    std::cout << "The cell is already full\n";
                }
            }
            else {
                std::cout << "The coordinates entered are not valid\n";
            }
        }

    } while (coordinatesAreValid == false);
    return coordinates;
}