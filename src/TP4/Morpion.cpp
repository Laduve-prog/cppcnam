#include "Morpion.h"
#include <iostream>
#include <string>



void Morpion::playRound() {
    //Increment the number of rounds
    round++;
    setCurrentPlayer();

    //ask the player x and y coordinates where to play the disc
    Point coordinates = currentPlayer.playMove(grid);

    //Place the disc in the grid
    playDisc(coordinates.x, coordinates.y);

    //display grid
    terminalDisplay.displayGrid(grid);


    //Check if the current player has won
    setIsFinished(winChecker.checkWin(coordinates.x, coordinates.y));

    if (isFinished == true) {
        terminalDisplay.displayWinnerMessage(currentPlayer);
    }

    //Check if there is still empty places in the grid
    if (isFinished == false) {
        setIsFinished(winChecker.checkDraw(round));
    }
}