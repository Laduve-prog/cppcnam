#include "WinChecker.h"
#include <iostream>
#include <string>

//Returns if the game is won
bool WinChecker::checkWin(int x, int y) const {
    for (int i = 0; i < grid.getNbColumns(); i++) {
        for (int j = 0; j < grid.getNbRows(); j++) {
            if (isLinearMatch(i, j, 1, 0) || isLinearMatch(i, j, 0, 1) || isLinearMatch(i, j, 1, 1) || isLinearMatch(i, j, -1, -1) || isLinearMatch(i, j, -1, 0) || isLinearMatch(i, j, 0, -1) || isLinearMatch(i, j, 1, -1) || isLinearMatch(i, j, -1, 1)) {
                return true;
            }
        }
    }

    return false;
}

//Returns if the game ended in a tie
bool WinChecker::checkDraw(int round) const {
    if (round == maxRound) {
        std::cout << "Match nul";
        return true;
    }
    else {
        return false;
    }
}

//Checks if there is a sequence of symbol ( with n symbol = winCondition) from the coordinates in a given direction
bool WinChecker::isLinearMatch(int xCoordinate, int yCoordinate, int stepx, int stepy) const {
    vector<vector<int>>tab = grid.getGrid();
    //Retrieve the value of the box for the check
    int startValue = tab[xCoordinate][yCoordinate];

    if (startValue == 0) {
        return false;
    }

    // Loop from 1 because we know that the first value is correct
    for (int i = 1; i < winCondition; i++) {
        // Checks if row and column indices are still within array bounds
        if ((xCoordinate + i * stepx >= 0 && xCoordinate + i * stepx < grid.getNbColumns()) && (yCoordinate + i * stepy >= 0 && yCoordinate + i * stepy < grid.getNbRows())) {
            //Check if different from the start value
            if (tab[xCoordinate + i * stepx][yCoordinate + i * stepy] != startValue) {
                return false;
            }
        }
        else {
            return false;
        }
    }
    return true;
}

bool WinChecker::checkForWin() {
    // Iterate through the grid to check for any empty cells
    for (int i = 0; i < grid.getNbRows(); i++) {
        for (int j = 0; j < grid.getNbColumns(); j++) {
            if (grid.checkIfCellIsEmpty(i, j) && grid.isValidMove({ i, j }, 1) || grid.checkIfCellIsEmpty(i, j) && grid.isValidMove({i,j}, 2)) {
                // If an empty cell is found, the game is not finished
                return false;
            }
        }
    }
    return true;
}

int WinChecker::getOthelloWinner() {
    vector<vector<int>>tab = grid.getGrid();
    // Initialize counters for the number of discs of each color
    int xDiscs = 0;
    int oDiscs = 0;

    // Iterate through the grid to count the number of discs of each color
    for (int i = 0; i < grid.getNbRows(); i++) {
        for (int j = 0; j < grid.getNbColumns(); j++) {

            if (tab[i][j] == 1) {
                xDiscs++;
            }
            else if (tab[i][j] == 2) {
                oDiscs++;
            }
        }
    }

    // If one player has more discs than the other, return true to indicate a win
    if (xDiscs > oDiscs) {
        return 1;
    }
    else if (oDiscs > xDiscs) {
        return 2;
    }
    // If the number of discs of each color is equal, the game is a draw
    else {
        return 3;
    }
}