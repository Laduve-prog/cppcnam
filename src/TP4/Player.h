#include <string>
#include <iostream>
#include "IPlayer.h"
#ifndef PLAYER_H
#define PLAYER_H
class Player : public IPlayer {
public:
    Player() {};
    Player(int id);
    inline int getId() const { return id; };
    virtual Point playMove(Grid grid) override;
private:
    int id;
};
#endif //PLAYER_H
