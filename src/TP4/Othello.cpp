#include "Othello.h"
#include "Game.h"
#include "Grid.h"
#include "WinChecker.h"
#include <vector>


 // Override the playRound function to implement the Othello game rules
void Othello::playRound() {

	//On incremente le nombre de round
	round++;
	setCurrentPlayer();

	// Point pMove = currentPlayer.playMove(grid);

	//std::string msg = "C'est au tour du joueur " + std::to_string(currentPlayer.getId()) + "\n" + "Veuillez saisir les coordonnees de la case (format : x y)" + "\n";
	//terminalDisplay.displayText(msg);
	//pMove = userInput.getPointValue();

	// Get the player's move
	Point pMove;
	do {
		std::string msg = "Your pawn must be adjacent to an opposing pawn";
		terminalDisplay.displayText(msg);
		pMove = currentPlayer.playMove(grid);
	} while (grid.isValidMove(pMove, currentPlayer.getId()) == false);
	playDisc(pMove.x, pMove.y);

	

	
	//Place the player's piece on the board and flip any captures pieces
	// grid.placeDisc(pMove.x , pMove.y, currentPlayer.getId());
	grid.flipDiscs(pMove, currentPlayer.getId());

	// Display the game board
	terminalDisplay.displayGrid(grid);

	// Check if the player has won the game
	if (winChecker.checkForWin()) {
		isFinished = true;
		std::cout << "Player " << winChecker.getOthelloWinner() << " has won the game!" << std::endl;
	}
}