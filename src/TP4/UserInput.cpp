#include "UserInput.h"
#include <iostream>
#include <any>

int UserInput::getIntValue() {
    int value;

    while (true) {
        std::cin >> value;
        if (cin.fail()) {
            td.displayErreurSaisie("User input is not an integer!");
            std::cin.clear(); //clear flags
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); //discard unread characters from the input buffer
        }
        else {
            break;
        }
    }

    return value;
}

Point UserInput::getPointValue() {
    Point p;
    
    while (true) {
        std::cin >> p.x >> p.y;
        if (cin.fail()) {
            td.displayErreurSaisie("User input is not an integer!");
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        else {
            break;
        }
    }

    return p;
}