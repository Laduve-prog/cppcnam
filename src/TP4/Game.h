#include <vector>
#include <iostream>
#include "Player.h"
#include "Grid.h"
#include "WinChecker.h"
#include "UserInput.h"
#include "TerminalDisplay.h"

#ifndef GAME_H
#define GAME_H

class Game
{
public:
    Game(int xGridSize, int yGridSize, int winCondition, std::vector<Player> players);
    void launch();

protected:
    Grid grid;
    Player currentPlayer;
    bool isFinished;
    int round;
    WinChecker winChecker;
    TerminalDisplay terminalDisplay;
    UserInput userInput;

    virtual void playRound() = 0;

    std::vector<Player> getPlayers() { return players; };
    const Grid& getGrid() const { return grid; };
    int getNbRounds() const { return round; };

    void setCurrentPlayer();
    void setIsFinished(bool b) { isFinished = b; }
    void playDisc(int xCoordinate, int yCoordinate);

private:
    std::vector<Player> players;
};

#endif // GAME_H