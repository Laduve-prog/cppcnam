#include "Grid.h"
#include <iostream>

Grid::Grid(int xGridSize, int yGridSize) {
    nbRows = xGridSize;
    nbColumns = yGridSize;

    std::vector<std::vector<int>> rowsVector;

    for (int row = 0; row < yGridSize; row++)
    {
        std::vector<int> colsVector;
        for (int col = 0; col < xGridSize; col++)
        {
            colsVector.push_back(0);
        }
        rowsVector.push_back(colsVector);
    }

    this->grid = rowsVector;
}

bool Grid::checkIfCellIsEmpty(int xCoordinate, int yCoordinate) const {
    if (this->grid[xCoordinate][yCoordinate] == 0) {
        return true;
    }
    else {
        return false;
    }
}

void Grid::placeDisc(int x, int y, int id)
{
    grid[x][y] = id;
}

bool Grid::validateCoordinate(int x, int y)
{
    if (x < this->getNbColumns() && y < this->getNbRows()) {
        return true;
    }
    return false;
}

int Grid::checkIfColumIsEmpty(int ColumNb) const
{
    int tmp = -1;
    int i = 0;

    while (i < nbColumns && this->checkIfCellIsEmpty(i, ColumNb) == true) {
        tmp = i;
        i++;
    }

    return tmp;
}

void Grid::flipDiscs(Point start, int currentPlayerId) {
    // Flip discs in all 8 directions
    //flipDiscsInDirection(start, currentPlayerId, -1, -1); // top-left
    //flipDiscsInDirection(start, currentPlayerId, -1, 0); // top
    //flipDiscsInDirection(start, currentPlayerId, -1, 1); // top-right
    //flipDiscsInDirection(start, currentPlayerId, 0, -1); // left
    //flipDiscsInDirection(start, currentPlayerId, 0, 1); // right
    //flipDiscsInDirection(start, currentPlayerId, 1, -1); // bottom-left
    //flipDiscsInDirection(start, currentPlayerId, 1, 0); // bottom
    //flipDiscsInDirection(start, currentPlayerId, 1, 1); // bottom-right
    for (int i = -1; i <= 1; i++) {
        for (int j = -1; j <= 1; j++) {
            flipDiscsInDirection(start, currentPlayerId, i, j);
        }
    }
}

bool Grid::isValidMove(Point start, int currentPlayerId) const {
    // Checks that the square is adjacent to an opponent's disc
    for (int i = -1; i <= 1; i++) {
        for (int j = -1; j <= 1; j++) {
            if (i == 0 && j == 0) continue;  // Ignore the central square

            int r = start.x + i;
            int c = start.y + j;
            if (r <= grid.size()-1 && c < grid.size()-1 && checkIfCellIsEmpty(r, c) == false && grid[r][c] != currentPlayerId) {
                // Checks if a token will be returned in the given direction
                if (willTokenBeTurned(start, currentPlayerId, i, j)) {
                    return true;
                }
            }
        }
    }
    return false;
}
bool Grid::willTokenBeTurned(Point start, int currentPlayerId, int i, int j) const{
    int r = start.x + i;
    int c = start.y + j;
    while (r >= 0 && r < grid.size()-1 && c >= 0 && c < grid.size()-1) {
        if (grid[r][c] == currentPlayerId) {
            return true;
        }
        else if (grid[r][c] == 0) {
            return false;
        }
        r += i;
        c += j;
    }
    return false;
}

// Flips the discs in the given direction starting from the given position
void Grid::flipDiscsInDirection(Point start, int currentPlayerId, int dx, int dy) {

    int x = start.x + dx;
    int y = start.y + dy;

    std::vector<Point> flipped;

    //Go through the grid until we reach an empty cell or the edge of the board
    while (x >= 0 && x < 8 && y >= 0 && y < 8 && (grid[x][y] != 0 && grid[x][y] != currentPlayerId)) {
        flipped.push_back({ x,y });
        x += dx;
        y += dy;
    }

    //If the traversel ended on an empty cell , it means that no discs can be flipped.
    //Otherwise , if it ended on a disc belonging to the player who placed the disc, flip all the discs that were encountrerd along the way
    if (x >= 0 && x < 8 && y >= 0 && y < 8 && grid[x][y] == currentPlayerId) {
        for (Point p : flipped) {
            grid[p.x][p.y] = currentPlayerId;
        }
    }
}