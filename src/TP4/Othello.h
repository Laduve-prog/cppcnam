#include "Grid.h"
#include <vector>
#include "Player.h"
#include "Game.h"

#ifndef OTHELLO_H
#define OTHELLO_H

class Othello : public Game
{
public:
    Othello(std::vector<Player> players) :Game(8, 8, 4, players){
		// Initialize the game board with starting pieces
		grid.placeDisc(3, 3, 1);
		grid.placeDisc(4, 4, 1);
		grid.placeDisc(3, 4, 2);
		grid.placeDisc(4, 3, 2);
	};

    virtual void playRound() override;
};

#endif //Othello_H
