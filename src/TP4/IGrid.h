#pragma once

// Grid interface
class IGrid {
public:
    virtual bool checkIfCellIsEmpty(int xCoordinate, int yCoordinate) const = 0;
    virtual void placeDisc(int x, int y, int id) = 0;
    virtual bool validateCoordinate(int x, int y) = 0;
};
