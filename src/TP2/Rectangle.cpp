#include "Rectangle.h"
#include  <iostream>

/**
 * @brief Construct a new Rectangle:: Rectangle object
 * 
 * @param x : Coordonée sur x du point supérieur gauche du rectangle.
 * @param y : Coordonée sur y du point supérieur gauche du rectangle.
 * @param longueur 
 * @param largeur 
 */
Rectangle::Rectangle(int x , int y , int longueur, int largeur) {
    this->longueur = longueur;
    this->largeur = largeur;
    pointSupGauche.x = x;
    pointSupGauche.y = y;
}

/**
 * @brief Detruit le rectangle
 * 
 */
Rectangle::~Rectangle(){}

/**
 * @brief Retourne la largeur du rectangle.
 * 
 * @return int 
 */
int Rectangle::getLargeur() const{
    return largeur;
}

/**
 * @brief Retourne la longueur du rectangle.
 * 
 * @return int 
 */
int Rectangle::getLongueur() const{
    return longueur;
}

/**
 * @brief Retourne le point supérieur gauche du rectangle.
 * 
 * @return Point 
 */
Point Rectangle::getPoint() const{
    return pointSupGauche;
}

/**
 * @brief Permet de modifier la largeur du rectangle.
 * 
 * @param value 
 */
inline void Rectangle::setLargeur(int value){
    this->largeur = value;
}

/**
 * @brief Permet de modifier la longueur du rectangle.
 * 
 * @param value 
 */
inline void Rectangle::setLongueur(int value){
    this->longueur = value;
}

/**
 * @brief Permet de modifier les coordonnées du point supérieur gauche du rectangle.
 * 
 * @param value 
 */
void Rectangle::setPoint(Point value){
    this->pointSupGauche = value;
}

/**
 * @brief Calcule et retourne le périmètre du rectangle.
 * 
 * @return int 
 */
inline int Rectangle::getPerimetre() const{
    return (longueur + largeur) * 2;
}

/**
 * @brief Calcule et retourne la surface du rectangle.
 * 
 * @return double 
 */
inline double Rectangle::getSurface() const{
    return (longueur * largeur);
}

/**
 * @brief Affiche les propriétés du rectangle.
 * 
 */
void Rectangle::afficher(){
    std::cout << "----- Propriete du rectangle ----- \n";
    std::cout << "Point superieur gauche du rectangle : [" << pointSupGauche.x << "," << pointSupGauche.y << "]"  << "\n";
    std::cout << " Longueur du rectangle : " << getLongueur() << "\n";
    std::cout << " Largeur du rectangle : " << getLargeur()  << "\n";
    std::cout << " Perimetre du rectangle : " << getPerimetre()  << "\n";
    std::cout << " Surface du rectangle : " << getSurface()  << "\n";
}