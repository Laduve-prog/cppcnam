#include "Point.h"

#ifndef RECTANGLE_H
#define RECTANGLE_H

class Rectangle{

    public:
        Rectangle(int x , int y, int longueur , int largeur);
        ~Rectangle();

        inline int getLongueur() const;
        inline int getLargeur() const;
        inline Point getPoint() const;

        inline void setLongueur(int value);
        inline void setLargeur(int value);
        void setPoint(Point point);

        inline int getPerimetre() const;
        inline double getSurface()const;

        void afficher();
    private:
       int longueur;
       int largeur;

       //Point supérieur gauche du rectangle
       Point pointSupGauche; 
    
};

#endif //RECTANGLE_H