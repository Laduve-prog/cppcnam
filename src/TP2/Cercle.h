#include "Point.h"

#ifndef CERCLE_H
#define CERCLE_H

class Cercle{

    public:
        Cercle(int x,int y,int diametre);
        ~Cercle();

        inline int getDiametre() const;
        inline Point getCentre() const;

        inline void setDiametre(int value);
        void setCentre(Point value);

        inline double getPerimetre() const;
        inline double getSurface() const;
        bool isOnCercle(Point p);
        bool isInCercle(Point p);
        int getDistanceWithCenter(Point p);

        void afficher();

    private:
        Point centre;
        int diametre;
};

#endif //CERCLE_H