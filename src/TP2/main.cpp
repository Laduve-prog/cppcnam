#include "Point.h"
#include "Cercle.h"
#include "Rectangle.h"
#include "Triangle.h"

int main(){

    //Test du rectangle
    Rectangle* r = new Rectangle(3,4,10,5);
    r->afficher();
    delete r;

    //Test du cercle
    Cercle* c = new Cercle(0,0,10);
    c->afficher();
    delete c;

    //Test du triangle
    Point a;
    a.x = 0;
    a.y = 1;
    Point b;
    b.x = 1;
    b.y = 1;
    Point d;
    d.x = 1;
    d.y = 0;

    Triangle* t = new Triangle(a,b,d);
    t->afficher();
    delete t;
}