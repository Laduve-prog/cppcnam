#define _USE_MATH_DEFINES
#include "Triangle.h"
#include <math.h>
#include <iostream>

/**
 * @brief Construire un nouveau triangle
 * 
 * @param a : Point
 * @param b : Point
 * @param c : Point
 */

Triangle::Triangle(Point a , Point b , Point c){
    this->a = a;
    this->b = b;
    this->c = c;

    cotes[0] = getLongueur(a,b);
    cotes[1] = getLongueur(a,c);
    cotes[2] = getLongueur(b,c);
}

/**
 * @brief Détruit le triangle
 * 
 */
Triangle::~Triangle(){}

void Triangle::setA(const Point p){
    this->a = p;
}

void Triangle::setB(Point p){
    this->b = p;
}

void Triangle::setC(const Point p){
    this->c = p;
}

/**
 * @brief Renvoie la longueur la plus longue du triangle
 * 
 * @return double
 */
double Triangle::getBase()const{

    double max = cotes[0];
    for(int i = 1; i < 3 ; i++){
        if(cotes[i] > max){
            max = cotes[i];
        }
    }

    return max;
}

/**
 * @brief Renvoie la surface du triangle
 * 
 * @return double : Surface du triangle
 */
double Triangle::getSurface()const{
    double s = 0.5*(cotes[0]+cotes[1]+cotes[2]);
    double aire = sqrt(s*(s-cotes[0])+(s-cotes[1])+(s-cotes[2]));
    return aire;
}

/**
 * @brief Renvoie la hauteur de la base du triangle 
 * 
 * @return double : Hauteur de la base du triangle
 */
double Triangle::getHauteur()const{
    return (this->getSurface()*2)/this->getBase();
}

/**
 * @brief Renvoie la distance entre deux points
 * 
 * @param p1 :Point
 * @param p2 :Point
 * @return double 
 */
double Triangle::getLongueur(Point p1 , Point p2) const{
    return sqrt(pow((p1.x - p2.x),2)+pow((p1.y - p2.y),2));
}

/**
 * @brief Renvoie si le triangle est isocele
 * 
 * @return true 
 * @return false 
 */
bool Triangle::isIsocele() const{
    return bool(cotes[0] == cotes[1] || cotes[0] == cotes[2] || cotes[1] == cotes[2]);
}

/**
 * @brief Renvoie si le triangle est Equilateral
 * 
 * @return true 
 * @return false 
 */
bool Triangle::isEquilateral() const{
    return bool(cotes[0] == cotes[1] && cotes[1]== cotes[2]);
}

/**
 * @brief Renvoie si le triangle est rectangle
 * 
 * @return true 
 * @return false 
 */
bool Triangle::isRectangle()const{
    //On note l'index du potentiel hypoténuse
    int j = 0;
    double tmp = 0;
    for(int i=0; i< 0;i++){
        if(cotes[i] == getBase()){
            j = i;
        }
        //Si pas d'hypoténuse on calcul leur carré et on l'ajoute a tmp
        else{
            tmp += pow(cotes[i],2);
        }
    }

    return bool(pow(cotes[j],2) == tmp);
}

/**
 * @brief Affiche les propriétés du triangle
 * 
 */
void Triangle::afficher()const{
        std::cout << "----- Propriete du Triangle ----- \n";
    std::cout << "Sommet A du triangle : [" << a.x << "," << a.y << "]"  << "\n";
    std::cout << "Sommet B du triangle : [" << b.x << "," << b.y << "]"  << "\n";
    std::cout << "Sommet C du triangle : [" << c.x << "," << c.y << "]"  << "\n";
    std::cout << "Base du triangle : " << getBase()  << "\n";
    std::cout << "Hauteur de la base du triangle: " << getHauteur()  << "\n";
    std::cout << "Longueur de l'arete AB : " << cotes[0] << "\n";
    std::cout << "Longueur de l'arete AC : " << cotes[1] << "\n";
    std::cout << "Longueur de l'arete BC : " << cotes[2] << "\n";
    std::cout << "Le triangle est il isocele? : " << isIsocele() << "\n";
    std::cout << "Le triangle est il equilateral? : " << isEquilateral() << "\n";
    std::cout << "Le triangle est il rectangle? : " << isRectangle() << "\n";
}

