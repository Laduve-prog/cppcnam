#include "Point.h"

#ifndef TRIANGLE_H
#define TRIANGLE_H

class Triangle{
    public:
        Triangle(Point a,Point b,Point c);
        ~Triangle();

        inline Point getA() const { return this->a; }
        inline Point getB() const { return this->b; }
        inline Point getC() const { return this->c; }

        void setA(const Point p);
        void setB(const Point p);
        void setC(const Point p);

        double getBase() const;
        double getHauteur() const;
        double getSurface() const;
        double getLongueur(Point p1 , Point p2) const;
        inline bool isIsocele() const;
        inline bool isEquilateral() const;
        inline bool isRectangle() const;

        void afficher() const;

    private: Point a ,b ,c;
            double cotes[3];
            double ab;
};

#endif //TRIANGLE_H