#define _USE_MATH_DEFINES
#include "Cercle.h"
#include <math.h>
#include <iostream>

Cercle::Cercle(int x, int y , int diametre){
    centre.x = x;
    centre.y = y;
    this->diametre = diametre;
}

Cercle::~Cercle(){}

/**
 * @brief Retourne le diametre du cercle.
 * 
 * @return int 
 */
int Cercle::getDiametre() const{
    return diametre;
}

/**
 * @brief Retourne le centre du cercle.
 * 
 * @return Point 
 */
Point Cercle::getCentre() const{
    return centre;
}

/**
 * @brief Permet de modifier la valeur du diametre.
 * 
 * @param value 
 */
void Cercle::setDiametre(int value) {
    this->diametre = value;
}

/**
 * @brief Permet de modifier le centre du cercle.
 * 
 * @param value 
 */
void Cercle::setCentre(Point value) {
    this->centre = value;
}

/**
 * @brief Calcule et Retourne le périmètre du cercle.
 * 
 * @return double 
 */
double Cercle::getPerimetre() const{
    return 2 * ( diametre/2 ) * M_PI;
}

/**
 * @brief Calcule et retourne la surface du cercle.
 * 
 * @return double 
 */
double Cercle::getSurface() const{
    return M_PI * pow((diametre/2),2);
}

/**
 * @brief Renvoie la distance entre le point et le centre du cercle.
 * 
 * @param p 
 * @return int 
 */
int Cercle::getDistanceWithCenter(Point p){
    Point c = this->getCentre();
    //On calcule la distance entre le point p et le centre du cercle
    return sqrt(pow((p.x - c.x),2)+pow((p.y - c.y),2));
}

/**
 * @brief Renvoie si le point appartient au périmètre du cercle.
 * 
 * @param p 
 * @return true 
 */
bool Cercle::isOnCercle(Point p) {
    //On calcule la distance entre le point p et le centre du cercle
    int distance = getDistanceWithCenter(p);
    //On compare la distance obtenu avec le rayon du cercle
    //Si la distance est égale au rayon le point est sur le cercle
    if(distance == (getDiametre()/2)){
        return true;
    }
    else{
        return false;
    }
}

/**
 * @brief Renvoie si le point est dans le cercle mais pas sur le cercle
 * 
 * @param p 
 */
bool Cercle::isInCercle(Point p){
    //On calcule la distance entre le point p et le centre du cercle
    int distance = getDistanceWithCenter(p);

    //Si la distance est inférieur au rayon le point est dans le cercle
    if(distance == (getDiametre()/2) || distance > (getDiametre()/2)){
        return false;
    }
    else{
        return true;
    }
}

void Cercle::afficher(){
    std::cout << "----- Propriete du Cercle -------- \n";
    std::cout << "Coordonnee du centre du cercle : [" << centre.x << "," << centre.y << "]\n";
    std::cout << "Diametre du cercle : " << getDiametre() << "\n";
    std::cout << "Perimetre du cercle : " << getPerimetre() << "\n";
    std::cout << "Surface du cercle : " << getSurface() << "\n";
}