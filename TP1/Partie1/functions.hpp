
int somme(int nb1 , int nb2);
void inverseIntPointeurs(int* nb1, int* nb2);
void inverseIntRef(int& nb1, int& nb2);
void affecteSommePointeurs(int* nb1, int* nb2, int* nb3);
void affecteSommeRef(int& nb1, int& nb2, int& nb3);
void triInsertion(int tab[],int size , bool isAsc);
void afficheTableau(int tab[], int size);