// TP1C++.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>
#include "functions.hpp"


int main()
{
    int a;
    int b;
    int c;

    //Test I.1.1
    std::cout << "Saisir un entier : \n";
    std::cin >> a;
    std::cout << "Saisir un deuxieme entier : \n";

    std::cin >> b;


    int som = somme(a, b);
    std::cout << "Somme des deux entiers : " << som << "\n";

    //Test I.1.2
    //inverseIntPointeurs(&a, &b);
    inverseIntRef(a, b);
    std::cout << "1er entier : " << a << "\n";
    std::cout << "2eme entier : " << b << "\n";

    //Test I.1.3
    std::cout << "1.1.3 avec pointeurs : \n";
    affecteSommePointeurs(&a, &b, &c);
    std::cout << "Nouvelle Valeur de C : " << c << "\n";

    //Reset de c
    c = 0;
    std::cout << "1.1.3 avec references : \n";
    affecteSommeRef(a, b, c);
    std::cout << "Nouvelle Valeur de C : " << c << "\n";

    //Test I.1.4
    const int size = 10;


    bool isAsc;
    std::cout << "Pour un tri croissant : 1 , pour un tri decroissant : 0 \n";
    std::cin >> isAsc;


    int tableau[size];

    for (int i = 0; i <= 9; i = i + 1) {
        srand(time(NULL));
        tableau[i] = rand() % 100;
    }

    //Affichage du tableau
    afficheTableau(tableau,size);

    //Tri du tableau
    triInsertion(tableau,size,isAsc);
    afficheTableau(tableau,size);
}