#include <iostream>

//I.1.1
int somme(int nb1, int nb2) {
    return nb1 + nb2;
}

//I1.2 -- Version Pointeurs
void inverseIntPointeurs(int* nb1, int* nb2) {
    int tmp = *nb1;
    *nb1 = *nb2;
    *nb2 = tmp;
}

// -- Version R�f�rence
void inverseIntRef(int& nb1, int& nb2) {
    int tmp = nb1;
    nb1 = nb2;
    nb2 = tmp;
}

//I1.3 -- Remplace 3eme int par la somme des deux premiers
//-- Version pointeurs
void affecteSommePointeurs(int* nb1, int* nb2, int* nb3) {
    *nb3 = somme(*nb1, *nb2);
}

//Version references
void affecteSommeRef(int& nb1, int& nb2, int& nb3) {
    nb3 = somme(nb1, nb2);
}

void triInsertion(int tab[], int size , bool isAsc) {
    int j;

    for (int i = 1; i < size; i++) {

        //Index pour la deuxi�me boucle
        j = i;

        //Tant que j sup�rieur � 0 et que la valeur pr�cedente et sup�rieur � la courante , on inverse les deux
        if (isAsc == true) {
            while (j > 0 && tab[j - 1] > tab[j]) {
                inverseIntRef(tab[j], tab[j - 1]);
                j--;
            }
        }
        else {
            while (j > 0 && tab[j - 1] < tab[j]) {
                inverseIntRef(tab[j], tab[j - 1]);
                j--;
            }
        }
    }
}

void afficheTableau(int tab[] , int size) {
    std::cout << "Affichage du tableau : \n";
    for (int i = 0; i < size; i = i + 1) {
        std::cout << tab[i] << " | ";
    }
}