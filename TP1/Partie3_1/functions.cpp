#include <string>

void format(std::string& nom, std::string& prenom) {
	//Traitement pour le prenom
	std::string tmp;
	tmp += toupper(prenom[0]);

	for (int i = 1; i < std::size(prenom); i++) {
		tmp += tolower(prenom[i]);
	}

	prenom = tmp;
	tmp = "";

	//Traitement pour le nom
	for (int i = 0; i < std::size(nom); i++) {
		tmp += toupper(nom[i]);
	}

	nom = tmp;
}