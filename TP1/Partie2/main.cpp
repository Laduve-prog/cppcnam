
#include <iostream>
#include "functions.hpp"

int main()
{
    std::cout << "Jeu de tennis!\n";

    int nbEchange1;
    std::cout << "Nombre d'echange remporte par le Joueur 1 : \n";
    std::cin >> nbEchange1;

    int nbEchange2;
    std::cout << "Nombre d'echange remporte par le Joueur 2 : \n";
    std::cin >> nbEchange2;

    determineScore(nbEchange1 , nbEchange2);
}

