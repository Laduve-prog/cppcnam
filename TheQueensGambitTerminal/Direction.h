#include "Coordinate.h"
#include <iostream>

enum DirectionEnum {
    TOP_RIGHT = 0,
    TOP_LEFT,
    LEFT,
    BOTTOM_LEFT,
    BOTTOM_RIGHT,
    RIGHT,
    BOTTOM,
    TOP
};

struct Direction {
	Coordinate value;
	std::string name;
};

const Direction directions[] = {
    {0, -1, "TOP_RIGHT"},
    {1, -1, "TOP_LEFT"},
    {1, 0, "LEFT"},
    {1, 1, "BOTTOM_LEFT"},
    {0, 1, "BOTTOM_RIGHT"},
    {-1, 1, "RIGHT"},
    {-1, 0, "BOTTOM"},
    {-1, -1, "TOP"}
};