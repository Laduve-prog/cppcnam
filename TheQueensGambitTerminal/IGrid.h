#pragma once
#include "Coordinate.h"

// Grid interface
class IGrid {
public:
    virtual bool checkIfCellIsEmpty(Coordinate c) const = 0;
    virtual void placeToken(int x, int y, int id) = 0;
    virtual bool validateCoordinate(int x, int y) = 0;
    virtual bool validateMove(Coordinate start, int currentPlayerId, Coordinate destination) = 0;
};
