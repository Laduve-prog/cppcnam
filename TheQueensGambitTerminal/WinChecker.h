#pragma once
#include "Grid.h"
#include "TokenCount.h"

#ifndef WINCHECKER_H
#define WINCHECKER_H

class WinChecker {

public:
	WinChecker(int winCondition, const Grid& grid) : winCondition(winCondition), grid(grid) { maxRound = grid.getNbColumns() * grid.getNbRows(); }
	bool checkWin() const;
	bool checkDraw(int round) const;
	bool checkForWin();
	int getOthelloWinner() const;
	int compareTokenCount(int xToken, int oToken) const;
	bool endCheckers() const;

private:
	int winCondition;
	const Grid& grid;
	int maxRound;

	bool isLinearMatch(Coordinate coordinate, Coordinate direction) const;
};

#endif // WINCHECKER_H