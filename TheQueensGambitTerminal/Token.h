#ifndef TOKEN_H
#define TOKEN_H
class Token {
public:
	Token(int playerId, bool dame) {
		this->dame = dame;
		this->playerId = playerId;
	};
	bool isDame() { return dame; }
	int getPlayerId() const { return playerId; }
	void setDame() { dame = true; }
private:
	bool dame;
	int playerId;
	//Coordinate coordinate;
};

#endif // TOKEN_H
