#include "Grid.h"
#include <vector>
#include "Player.h"
#include "Game.h"

#ifndef OTHELLO_H
#define OTHELLO_H

class Othello : public Game
{
public:
    Othello() :Game(8, 8, 4){
		// Initialize the game board with starting pieces
		Coordinate token1{ 3, 3 };
		Coordinate token2{ 4, 4 };
		Coordinate token3{ 3, 4 };
		Coordinate token4{ 4, 3 };

		grid.placeToken(token1, Token{ 1, false });
		grid.placeToken(token2, Token{ 1, false });
		grid.placeToken(token3, Token{ 2, false });
		grid.placeToken(token4, Token{ 2, false });
	};
	void saveGame();
	void loadGame();
    virtual void playRound() override;
};

#endif //Othello_H
