#include "Othello.h"
#include "Game.h"
#include "Grid.h"
#include "WinChecker.h"
#include <vector>
#include <fstream>


 // Override the playRound function to implement the Othello game rules
void Othello::playRound() {
	string resp;
	std::cout << "voulez vous charger la partie ?" << endl;
	std::cin >> resp;
	if (resp == "oui") {
		loadGame();
		terminalDisplay.displayGrid(grid);
	}
	else {
		saveGame();
	}
	//On incremente le nombre de round
	round++;
	setCurrentPlayer();

	// Get the player's move
	Coordinate pMove;
	do {
		std::string msg = "Your pawn must be adjacent to an opposing pawn";
		terminalDisplay.displayText(msg);
		pMove = currentPlayer.playMove(grid);
	} while (grid.isValidMove(pMove, currentPlayer.getId()) == false);
	playToken(pMove);

	

	
	//Place the player's piece on the board and flip any captures pieces
	grid.flipTokens(pMove, currentPlayer.getId());

	// Display the game board
	terminalDisplay.displayGrid(grid);

	// Check if the player has won the game
	if (winChecker.checkForWin()) {
		isFinished = true;
		terminalDisplay.displayWinnerMessage(currentPlayer);
	}
}
void Othello::saveGame() {
    // Open a file stream to save the game state
    std::ofstream saveFile("othello_game_state.bin", ios::binary);
    if (saveFile.is_open()) {
        // Write the current round number to the file
        saveFile.write((char*)&round, sizeof(int));
        // Write the current player id to the file
        int nbround = round;
        saveFile.write((char*)&nbround, sizeof(int));

        // Write the grid state to the file
        vector<vector<Token>> gridState = grid.getGrid();
        for (int i = 0; i < grid.getNbRows(); i++) {
            for (int j = 0; j < grid.getNbColumns(); j++) {
                int playerId = gridState[i][j].getPlayerId();
                saveFile.write((char*)&playerId, sizeof(int));
            }
        }

        // Close the file stream
        saveFile.close();
    }
    else {
        // If the file couldn't be opened, print an error message
        cout << "Error: Could not save game state." << endl;
    }
}
// Load a saved game state from disk
void Othello::loadGame() {
    // Open a file stream to load the game state
    ifstream loadFile("othello_game_state.bin", ios::binary);
    if (loadFile.is_open()) {
        // Read the current round number from the file
        loadFile.read((char*)&round, sizeof(int));
        // Read the current player id from the file
        int nbround;
        loadFile.read((char*)&nbround, sizeof(int));
        round = nbround;

        // Read the grid state from the file
        for (int i = 0; i < grid.getNbRows(); i++) {
            for (int j = 0; j < grid.getNbColumns(); j++) {
                int tokenId;
                loadFile.read((char*)&tokenId, sizeof(int));
                Coordinate posToken{ i , j };
                grid.placeToken(posToken, Token{ tokenId, false });
            }
        }

        // Close the file stream
        loadFile.close();
    }
    else {
        // If the file couldn't be opened, print an error message
        cout << "Error: Could not load game state." << endl;
    }
}