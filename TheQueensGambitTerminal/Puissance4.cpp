#include "Puissance4.h"
#include <iostream>
#include <string>
#include <fstream>

void Puissance4::playRound()
{
    int columnNb;
    int tmp = -1;

    string resp;
    std::cout << "voulez vous charger la partie ?" << endl;
    std::cin >> resp;
    if (resp == "oui") {
        loadGame();
        terminalDisplay.displayGrid(grid);
    }
    else {
        saveGame();
    }
    //Increment the number of rounds
    round++;

    setCurrentPlayer();


    do {
        
        
        std::cout << "It's the player's turn " << currentPlayer.getId() << endl;
        std::cout << "Please enter the column" << endl;
        std::cin >> columnNb;
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        if (cin.fail()) {
            std::cout << "User input is not an integer" << endl;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        else {

            Coordinate c{ 0 , columnNb };
            if (grid.validateCoordinate(c) == true) {
                tmp = grid.checkIfColumIsEmpty(columnNb);
            }
            else {
                std::cout << "The coordinates entered are not valid" << endl;
            }
        }

    } while (tmp == -1);

    //Place the Token in the grid
    Coordinate tokenCoordinate{tmp,columnNb};
    playToken(tokenCoordinate);

    //display grid
    terminalDisplay.displayGrid(grid);

    //Check if the current player has won
    setIsFinished(winChecker.checkWin());

    if (isFinished == true) {
        terminalDisplay.displayWinnerMessage(currentPlayer);
    }

    //Check if there is still empty places in the grid
    if (isFinished == false) {
        setIsFinished(winChecker.checkDraw(round));
    }
}
void Puissance4::saveGame() {
    // Open a file stream to save the game state
    std::ofstream saveFile("puissance4_game_state.bin", ios::binary);
    if (saveFile.is_open()) {
        // Write the current round number to the file
        saveFile.write((char*)&round, sizeof(int));
        // Write the current player id to the file
        int nbround = round;
        saveFile.write((char*)&nbround, sizeof(int));

        // Write the grid state to the file
        vector<vector<Token>> gridState = grid.getGrid();
        for (int i = 0; i < grid.getNbColumns(); i++) {
            for (int j = 0; j < grid.getNbRows(); j++) {
                int playerId = gridState[i][j].getPlayerId();
                saveFile.write((char*)&playerId, sizeof(int));
            }
        }

        // Close the file stream
        saveFile.close();
    }
    else {
        // If the file couldn't be opened, print an error message
        cout << "Error: Could not save game state." << endl;
    }
}

// Load a saved game state from disk
void Puissance4::loadGame() {
    // Open a file stream to load the game state
    ifstream loadFile("puissance4_game_state.bin", ios::binary);
    if (loadFile.is_open()) {
        // Read the current round number from the file
        loadFile.read((char*)&round, sizeof(int));
        // Read the current player id from the file
        int nbround;
        loadFile.read((char*)&nbround, sizeof(int));
        round = nbround;

        // Read the grid state from the file
        for (int i = 0; i < grid.getNbColumns(); i++) {
            for (int j = 0; j < grid.getNbRows(); j++) {
                int tokenId;
                loadFile.read((char*)&tokenId, sizeof(int));
                Coordinate posToken{ i , j };
                grid.placeToken(posToken, Token{ tokenId, false });
            }
        }

        // Close the file stream
        loadFile.close();
    }
    else {
        // If the file couldn't be opened, print an error message
        cout << "Error: Could not load game state." << endl;
    }
}
