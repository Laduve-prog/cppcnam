#include "Grid.h"
#include "Coordinate.h"
#include "TokenCount.h"
#include "Token.h"
#include <iostream>

Grid::Grid(int xGridSize, int yGridSize) {
    nbRows = xGridSize;
    nbColumns = yGridSize;

    std::vector<std::vector<Token>> rowsVector;

    for (int row = 0; row < yGridSize; row++)
    {
        std::vector<Token> colsVector;
        for (int col = 0; col < xGridSize; col++)
        {
            colsVector.push_back(Token{0,false});
        }
        rowsVector.push_back(colsVector);
    }

    this->grid = rowsVector;
}

bool Grid::checkIfCellIsEmpty(Coordinate c) const {
    if (this->grid[c.x][c.y].getPlayerId() == 0) {
        return true;
    }
    else {
        return false;
    }
}

void Grid::placeToken(Coordinate c, Token token)
{
    grid[c.x][c.y] = token;
}

bool Grid::validateCoordinate(Coordinate c)
{
    if (c.x < this->getNbColumns() && c.y < this->getNbRows()) {
        return true;
    }
    return false;
}
bool Grid::possibleTake(Coordinate start, int currentPlayerId, Coordinate destination) {
    if (grid[start.x][start.y].isDame()) {
        int x_diff = abs(destination.x - start.x);
        int y_diff = abs(destination.y - start.y);
        if (x_diff != y_diff) {
            return false;
        }
        int x_step = (destination.x > start.x) ? 1 : -1;
        int y_step = (destination.y > start.y) ? 1 : -1;
        int x = start.x + x_step;
        int y = start.y + y_step;
        //V�rifie qu'il n'y a pas de pion de la m�me couleur sur le chemin
        while (x != destination.x) {
            if (grid[x][y].getPlayerId() == currentPlayerId) {
                return false;
            }
            x += x_step;
            y += y_step;
        }
        // V�rifie qu'il y a des pions � manger sur le chemin
        x = start.x + x_step;
        y = start.y + y_step;
        bool foundEnemy = false;
        while (x != destination.x) {
            if (grid[x][y].getPlayerId() != 0 && grid[x][y].getPlayerId() != currentPlayerId) {
                foundEnemy = true;
            }
            else if (grid[x][y].getPlayerId() == 0 && foundEnemy) {
                return false;
            }
            x += x_step;
            y += y_step;
        }
        if (!foundEnemy) {
            return false;
        }
    }
    else if (currentPlayerId == 2) {
        if ((start.x == destination.x - 2 && (start.y == destination.y + 2 || start.y == destination.y - 2))
            && (grid[destination.x - 1][destination.y + 1].getPlayerId() == 1 || grid[destination.x - 1][destination.y - 1].getPlayerId() == 1) ||
            (start.x == destination.x + 2 && (start.y == destination.y + 2 || start.y == destination.y - 2))
            && (grid[destination.x + 1][destination.y + 1].getPlayerId() == 1 || grid[destination.x + 1][destination.y - 1].getPlayerId() == 1)
            ) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        if ((start.x == destination.x + 2 && (start.y == destination.y + 2 || start.y == destination.y - 2))
            && (grid[destination.x + 1][destination.y + 1].getPlayerId() == 2 || grid[destination.x + 1][destination.y - 1].getPlayerId() == 2) ||
            (start.x == destination.x - 2 && (start.y == destination.y + 2 || start.y == destination.y - 2))
            && (grid[destination.x - 1][destination.y + 1].getPlayerId() == 2 || grid[destination.x - 1][destination.y - 1].getPlayerId() == 2)
            ) {
            return true;
        }
        else {
            return false;
        }
    }

    return false;
}
bool Grid::validateMove(Coordinate start, int currentPlayerId, Coordinate destination) {
    if (grid[start.x][start.y].isDame()) {
        // Validation pour les dames
        int x_diff = abs(destination.x - start.x);
        int y_diff = abs(destination.y - start.y);
        if (x_diff != y_diff) {
            return false;
        }
        int x_step = (destination.x > start.x) ? 1 : -1;
        int y_step = (destination.y > start.y) ? 1 : -1;
        int x = start.x + x_step;
        int y = start.y + y_step;

        //V�rifie qu'il n'y a pas de pion de la m�me couleur sur le chemin
        while (x != destination.x) {
            if (grid[x][y].getPlayerId() == currentPlayerId) {
                return false;
            }
            x += x_step;
            y += y_step;
        }
        return true;
    }
    else if (currentPlayerId == 2) {
        if ((start.x == destination.x + 1 && (start.y == destination.y + 1 || start.y == destination.y -1)) || 
            possibleTake(start, currentPlayerId, destination)) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        if ((start.x == destination.x - 1 && (start.y == destination.y + 1 || start.y == destination.y - 1)) || 
            possibleTake(start, currentPlayerId, destination)
            ) {
            return true;
        }
        else {
            return false;
        }
    }
    
    return false;
}

int Grid::checkIfColumIsEmpty(int ColumnNb) const
{
    int tmp = -1;
    Coordinate cellPos = { 0 , ColumnNb };

    while (cellPos.x < nbColumns && this->checkIfCellIsEmpty(cellPos) == true) {
        tmp = cellPos.x;
        cellPos.x++;
    }

    return tmp;
}

void Grid::flipTokens(Coordinate start, int currentPlayerId) {
    // Flip Tokens in all 8 directions
    for (int i = -1; i <= 1; i++) {
        for (int j = -1; j <= 1; j++) {
            flipTokensInDirection(start, currentPlayerId, i, j);
        }
    }
}
bool Grid::takeToken(Coordinate start, Coordinate destination) {
    if (start.x == destination.x - 2 && start.y == destination.y + 2) {
        removeToken(Coordinate{ destination.x - 1,destination.y + 1 });
        return true;
    }
    else if (start.x == destination.x - 2 && start.y == destination.y - 2) {
        removeToken(Coordinate{ destination.x - 1,destination.y - 1 });
        return true;
    }
    else if (start.x == destination.x + 2 && start.y == destination.y + 2) {
        removeToken(Coordinate{ destination.x + 1,destination.y + 1 });
        return true;
    }
    else if (start.x == destination.x + 2 && start.y == destination.y - 2) {
        removeToken(Coordinate{ destination.x + 1,destination.y - 1 });
        return true;
    }
    return false;
}
bool Grid::isValidMove(Coordinate start, int currentPlayerId) const {
    // Checks that the square is adjacent to an opponent's Token
    for (int i = -1; i <= 1; i++) {
        for (int j = -1; j <= 1; j++) {
            if (i == 0 && j == 0) continue;  // Ignore the central square

            Coordinate position;
            position.x = start.x + i;
            position.y = start.y + j;

            if (position.x <= grid.size()-1 && position.y < grid.size()-1 && checkIfCellIsEmpty(position) == false && grid[position.x][position.y].getPlayerId() != currentPlayerId) {
                // Checks if a token will be returned in the given direction
                if (willTokenBeTurned(start, currentPlayerId, i, j)) {
                    return true;
                }
            }
        }
    }
    return false;
}
bool Grid::willTokenBeTurned(Coordinate start, int currentPlayerId, int i, int j) const{
    int r = start.x + i;
    int c = start.y + j;
    while (r >= 0 && r < grid.size()-1 && c >= 0 && c < grid.size()-1) {
        if (grid[r][c].getPlayerId() == currentPlayerId) {
            return true;
        }
        else if (grid[r][c].getPlayerId() == 0) {
            return false;
        }
        r += i;
        c += j;
    }
    return false;
}

// Flips the Tokens in the given direction starting from the given position
void Grid::flipTokensInDirection(Coordinate start, int currentPlayerId, int dx, int dy) {

    int x = start.x + dx;
    int y = start.y + dy;

    std::vector<Coordinate> flipped;

    //Go through the grid until we reach an empty cell or the edge of the board
    while (x >= 0 && x < 8 && y >= 0 && y < 8 && (grid[x][y].getPlayerId() != 0 && grid[x][y].getPlayerId() != currentPlayerId)) {
        flipped.push_back({ x,y });
        x += dx;
        y += dy;
    }

    //If the traversel ended on an empty cell , it means that no Tokens can be flipped.
    //Otherwise , if it ended on a Token belonging to the player who placed the Token, flip all the Tokens that were encountrerd along the way
    if (x >= 0 && x < 8 && y >= 0 && y < 8 && grid[x][y].getPlayerId() == currentPlayerId) {
        for (Coordinate p : flipped) {
            grid[p.x][p.y] = Token{ currentPlayerId,false };
        }
    }
}

void Grid::moveToken(Coordinate start, Token token, Coordinate destination) {
    removeToken(start);
    grid[destination.x][destination.y] = token;
}
bool Grid::isTokenOwner(Coordinate token, int currentPlayerId) const {
    if (grid[token.x][token.y].getPlayerId() == currentPlayerId) {
        return true;
    }
    else {
        return false;
    }
}
void Grid::removeToken(Coordinate token) {
    grid[token.x][token.y] = Token{ 0,false };
}

TokenCount Grid::countTokens() const {
    TokenCount TokenCount;
    TokenCount.xTokens = 0;
    TokenCount.oTokens = 0;

    for (int i = 0; i < getNbRows(); i++) {
        for (int j = 0; j < getNbColumns(); j++) {
            if (grid[i][j].getPlayerId() == 1) {
                TokenCount.xTokens++;
            }
            else if (grid[i][j].getPlayerId() == 2) {
                TokenCount.oTokens++;
            }
        }
    }
    return TokenCount;
};
