#include <string>
#include <iostream>
#include "Player.h"
#include "Coordinate.h"
#include "Grid.h"
#include "Game.h"
#include "sstream"
using namespace std;

Player::Player(int id) {
    this->id = id;
}

Coordinate Player::playMove(Grid grid)
{
    bool coordinatesAreValid = false;
    Coordinate coordinates;
    do {

        std::cout << "It's the player's turn " << id << endl;
        std::cout << "Please enter the coordinates( format: x y )" << endl;
        std::cin >> coordinates.x >> coordinates.y;

        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        if (cin.fail()) {
            std::cout << "User input is not an integer" << endl;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        else {
            if (grid.validateCoordinate(coordinates)) {
                if (grid.checkIfCellIsEmpty(coordinates) == true) {
                    coordinatesAreValid = true;
                }
                else {
                    std::cout << "The cell is already full" << endl;
                }
            }
            else {
                std::cout << "The coordinates entered are not valid" << endl;
            }
        }

    } while (coordinatesAreValid == false);
    return coordinates;
}
Coordinate Player::checkMove(Grid grid) { // Est-ce bien et au bon endroit ? 
    bool coordinatesAreValid = false;
    Coordinate coordinates;
    do {
        std::cout << "Please enter the coordinates( format: x y )" << endl;
        std::cin >> coordinates.x >> coordinates.y;
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        if (cin.fail()) {
            std::cout << "User input is not an integer\n";
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        else {
            if (grid.validateCoordinate(coordinates)) {
                if (grid.checkIfCellIsEmpty(coordinates) == true || grid.isTokenOwner(coordinates, id) == false) {
                    coordinatesAreValid = true;
                }
                else {
                    std::cout << "The cell is already full\n";
                }
            }
            else {
                std::cout << "The coordinates entered are not valid\n";
            }
        }

    } while (coordinatesAreValid == false);
    return coordinates;
}
Coordinate Player::useToken(Grid grid) // Est-ce bien et au bon endroit ? 
{
    bool coordinatesAreValid = false;
    Coordinate coordinates;
    do {
        std::cout << "It's the player's turn " << id << endl;
        std::cout << "Please enter the coordinates( format: x y )" << endl;
        std::cin >> coordinates.x >> coordinates.y;
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        if (cin.fail()) {
            std::cout << "User input is not an integer\n";
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        else {
            if (grid.validateCoordinate(coordinates)) {
                if (grid.isTokenOwner(coordinates, id)) {
                    coordinatesAreValid = true;
                }
            }
            else {
                std::cout << "The coordinates entered are not valid\n";
            }
        }

    } while (coordinatesAreValid == false);
    return coordinates;
}
