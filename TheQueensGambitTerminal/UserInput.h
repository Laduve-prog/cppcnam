#include "TerminalDisplay.h"

#ifndef USERINPUT_H
#define USERINPUT_H

class UserInput {
public:
	UserInput() {}
	int getIntValue();
	Coordinate getCoordinateValue();
	TerminalDisplay td;
};

#endif //USERINPUT_H
