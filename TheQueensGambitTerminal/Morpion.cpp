#include "Morpion.h"
#include <iostream>
#include <string>
#include <fstream>


void Morpion::playRound() {
    string resp;
    std::cout << "voulez vous charger la partie ?" << endl;
    std::cin >> resp;
    if (resp == "oui") {
        loadGame();
        terminalDisplay.displayGrid(grid);
    }
    else {
        saveGame();
    }
    //Increment the number of rounds
    round++;
    setCurrentPlayer();

    //ask the player x and y coordinates where to play the Token
    Coordinate coordinates = currentPlayer.playMove(grid);

    //Place the Token in the grid
    playToken(coordinates);

    //display grid
    terminalDisplay.displayGrid(grid);


    //Check if the current player has won
    setIsFinished(winChecker.checkWin());

    if (isFinished == true) {
        terminalDisplay.displayWinnerMessage(currentPlayer);
    }

    //Check if there is still empty places in the grid
    if (isFinished == false) {
        setIsFinished(winChecker.checkDraw(round));
    }
}
void Morpion::saveGame() {
    // Open a file stream to save the game state
    std::ofstream saveFile("morpion_game_state.bin", ios::binary);
    if (saveFile.is_open()) {
        // Write the current round number to the file
        saveFile.write((char*)&round, sizeof(int));
        // Write the current player id to the file
        int nbround = round;
        saveFile.write((char*)&nbround, sizeof(int));

        // Write the grid state to the file
        vector<vector<Token>> gridState = grid.getGrid();
        for (int i = 0; i < grid.getNbRows(); i++) {
            for (int j = 0; j < grid.getNbColumns(); j++) {
                int playerId = gridState[i][j].getPlayerId();
                saveFile.write((char*)&playerId, sizeof(int));
            }
        }

        // Close the file stream
        saveFile.close();
    }
    else {
        // If the file couldn't be opened, print an error message
        cout << "Error: Could not save game state." << endl;
    }
}
// Load a saved game state from disk
void Morpion::loadGame() {
    // Open a file stream to load the game state
    ifstream loadFile("morpion_game_state.bin", ios::binary);
    if (loadFile.is_open()) {
        // Read the current round number from the file
        loadFile.read((char*)&round, sizeof(int));
        // Read the current player id from the file
        int nbround;
        loadFile.read((char*)&nbround, sizeof(int));
        round = nbround;

        // Read the grid state from the file
        for (int i = 0; i < grid.getNbRows(); i++) {
            for (int j = 0; j < grid.getNbColumns(); j++) {
                int tokenId;
                loadFile.read((char*)&tokenId, sizeof(int));
                Coordinate posToken{ i , j };
                grid.placeToken(posToken, Token{ tokenId, false });
            }
        }

        // Close the file stream
        loadFile.close();
    }
    else {
        // If the file couldn't be opened, print an error message
        cout << "Error: Could not load game state." << endl;
    }
}