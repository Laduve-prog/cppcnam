#include "TerminalDisplay.h"
#include "Grid.h"
#include "Player.h"
#include <iostream>

//Manages the grid display
void TerminalDisplay::displayGrid(const Grid grid) const
{
    std::cout << endl << endl;
    std::cout << "### Grid Display ###" << endl << endl;

    //Display nb
    std::cout << "  ";
    for (int i = 0; i < grid.getNbRows(); i++) {
        std::cout << i << "   ";
    }
    std::cout << std::endl;

    for (int i = 0; i < grid.getNbColumns(); i++)
    {
        //Display nb
        std::cout << i << " ";

        for (int j = 0; j < grid.getNbRows(); j++)
        {
            if (grid.getGrid()[i][j].getPlayerId() == 0)
            {
                std::cout << " ";
            }
            else
            {
                if (grid.getGrid()[i][j].getPlayerId() == 1)
                    std::cout << "X";
                else
                    std::cout << "O";
            }

            if (j == grid.getNbRows() - 1)
            {
                std::cout << std::endl;
            }
            else
            {
                std::cout << " | ";
            }
        }
    }

    std::cout << endl;
}

void TerminalDisplay::displayWinnerMessage(Player currentPlayer) const {

    std::cout << endl << "### Congratulations player " << currentPlayer.getId() << endl << endl << endl;

}

void TerminalDisplay::showGameSelectionMenu() {
    std::cout << "######## Game mode selection ########" << endl << endl << endl;
    std::cout << "Please enter the value that corresponds to the game" << endl;
    std::cout << "Choice of game: " << endl << "1: Tic Tac Toe " << endl << "2: Connect 4" << endl <<"3: Othello" << endl << "4: Checkers" << endl << "Others: Exit" << endl;
}

void TerminalDisplay::displayErreurSaisie(string msg) {
    std::cout << "Input Error : " << msg << endl;
}

void TerminalDisplay::displayText(string msg) {
    std::cout << msg << endl;
}
