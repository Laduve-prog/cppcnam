#include "Grid.h"
#include <vector>
#include "Player.h"
#include "Game.h"

#ifndef MORPION_H
#define MORPION_H

class Morpion : public Game
{
public:
    Morpion() :Game(3, 3, 3) {};
    virtual void playRound() override;
    void saveGame();
    void loadGame();
};

#endif //MORPION_H
